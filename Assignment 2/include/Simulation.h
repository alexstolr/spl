/*
 * UniCoffeeShop2.h

 *
 *  Created on: Nov 17, 2013
 *      Author: alexstol
 */

#ifndef SIMULATION_H_
#define SIMULATION_H_
#include <string>
#include <vector>
#include "../include/Customer.h"
#include "../include/Customers.h"
#include "../include/VipCustomer.h"
#include "../include/RegularCustomer.h"
#include "../include/Menu.h"
#include "../include/AppLogger.h"
#include "../include/LoggerParams.h"
typedef unsigned int uint;
using namespace std;

class Simulation{

private:
	Menu * menu;
	vector <vector<string> > prodVec;
	vector <vector<string> > suppVec;
	double profit;
	double revenue;
	string prodLine;
	string suppLine;
	Customers * newCustomer;

public:
	Simulation(char* productsFile, char* suppliersFile, char* eventsFile);
	void readNextEvent(string eventsLine);
	void registerEvent(string registerEvent);
	void purchaseEvent(string purchaseEvent);
	void suppUpdateEvent(string suppUpdateEvent);
	void endSimulation();
	~Simulation();
};





#endif /* SIMULATION_H_*/
