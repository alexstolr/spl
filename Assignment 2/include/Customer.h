/*
 * Customers.h

 *
 *  Created on: Nov 17, 2013
 *      Author: alexstol
 */

#ifndef CUSTOMER_H_
#define CUSTOMER_H_
#include <string>
#include <vector>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using namespace std;


 class Customer{
public:
	virtual double computeProductPrice(double originalPrice) = 0;
	virtual string getName() const=0;
	virtual string getFavouriteProduct() const=0;
	virtual ~Customer();
};






#endif  /* CUSTOMER_H_ */
