/*
 * Product.h

 *
 *  Created on: Nov 23, 2013
 *      Author: alexstol
 */

#ifndef PRODUCT_H_
#define PRODUCT_H_
#include "../include/Ingredient.h"

class Product{
public:
	Product(string product);
	void addToPrice(double p);
	void updatePrice(double p);
	double getPrice();
	string getProduct();
	vector<Ingredient *>& getIngredients();
	~Product();


private:
	string product;
	vector<Ingredient *> Ingredients;
	double price;

};



#endif /* PRODUCT_H_ */
