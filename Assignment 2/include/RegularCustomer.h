/*
 * RegularCustomer.h
 *
 *  Created on: Nov 29, 2013
 *      Author: alexstol
 */

#ifndef REGULARCUSTOMER_H_
#define REGULARCUSTOMER_H_
#include "../include/Customer.h"

class RegularCustomer :public Customer{
public: double computeProductPrice(double originalPrice);
RegularCustomer( string name,  string product, const string img);
string getName() const;
string getFavouriteProduct() const;
string getImg();
 virtual ~RegularCustomer();
private:
 string _name;
 string _favProduct;
 string _img;
};


#endif /* REGULARCUSTOMER_H_ */
