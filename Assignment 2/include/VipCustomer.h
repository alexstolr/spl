/*
 * VipCustomer.h
 *
 *  Created on: Nov 29, 2013
 *      Author: alexstol
 */

#ifndef VIPCUSTOMER_H_
#define VIPCUSTOMER_H_
#include "../include/Customer.h"

class VipCustomer :public Customer{
public:	VipCustomer(const  string name,const string product,const string img);
double computeProductPrice(double originalPrice);
string getName() const;
string getFavouriteProduct() const;
string getImg();
virtual ~VipCustomer();
private:
 string _name;
 string _favProduct;
string _img;
};



#endif /* VIPCUSTOMER_H_ */
