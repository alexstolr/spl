/*
 * Ingredient.h
 *
 *  Created on: Nov 23, 2013
 *      Author: alexstol
 */

#ifndef INGREDIENT_H_
#define INGREDIENT_H_
#include <vector>
#include <string>
using namespace std;

class Ingredient{
public:
	Ingredient(string ingredient, string supplier, double price);
	double getPrice();
	string getIngredient();
	string getSupplier();
	void setPrice(const double p);
	void setSupplier(const string s);
	~Ingredient();
private:
	string _ingredient;
	string _supplier;
	double _price;

};



#endif /* INGREDIENT_H_ */
