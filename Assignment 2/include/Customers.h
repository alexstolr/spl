/*
 * Customers.h
 *
 *  Created on: Nov 29, 2013
 *      Author: alexstol
 */

#ifndef CUSTOMERS_H_
#define CUSTOMERS_H_
#include "VipCustomer.h"

class Customers {
public: Customers();
~Customers();
vector<Customer *>& getCustomers();
private:
vector<Customer *> m_customers;
};


#endif /* CUSTOMERS_H_ */
