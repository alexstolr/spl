/*
 * Menu.h
 *
 *  Created on: Nov 23, 2013
 *      Author: alexstol
 */

#ifndef MENU_H_
#define MENU_H_
#include "../include/Product.h"
#include "../include/AppLogger.h"
#include "../include/LoggerParams.h"
#include <stdlib.h>
using namespace std;

class Menu
{
private:
	vector<Product*>* inProducts;//products included in the menu.
	vector<Product*>* exProducts; //products excluded from the menu.
	vector<Ingredient*>* allIngredients;
public:
	Menu();
	void generateMenu(vector <vector<string> > prodVec, vector <vector<string> > suppVec);
	int updateMenu(string supplier, string ingredient, double newPrice);
	double findIngredientPrice(string ingredient,vector <vector<string> > suppVec);
	double FindProductPrice(string product);
	string findIngredientSupplier(string ingredient, vector <Ingredient* >& suppVec, double price);
	double findIngredientPrice(string ingredient,vector <Ingredient* >& suppVec, double newPrice,string supplier);
	string findIngredientSupplier(string ingredient, vector<vector<string> > suppVec, double price);
	vector<Product*> * getInProducts();
	vector<Product*> * getExProducts();
	~Menu();
};


#endif /* MENU_H_ */
