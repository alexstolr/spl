/*
 * Main.cpp



 *
 *  Created on: Nov 18, 2013
 *      Author: alexstol
 */
#include "../include/Simulation.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits>
using namespace std;

int main(int argc, char **argv) {
	// checks if arguments are OK.
	for(int i = 1 ; i < 5 ; i++){
		fstream checkArg;
		checkArg.open(argv[i]);
		if(checkArg.fail()){ // if reading of argument[i] has failed.
			//turns of the argument[i] to string
			stringstream ss;
			string s;
			char * c = argv[i];
			ss << c;
			ss >> s;
			CAppLogger::Instance().Log(s +" not found.",Poco::Message::PRIO_CRITICAL);
			cout << s << " not found." << endl; // ******************delete later!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			return -1;
		}
	}
	// arguments are OK --> read and handle configuration file.
	string confLine;
	string logFileName;
	fstream configurations;
	configurations.open(argv[1]);// 1 = configuration file.
	getline(configurations, confLine); //read first line from configuration file (into confLine).
	logFileName=confLine.substr(confLine.find("=") + 2);// +2 to get rid of " ".
	getline(configurations, confLine);
	confLine = confLine.substr(confLine.find("=") + 2);
	uint filePriority=atoi(confLine.c_str()); //convert file priority given as string to an integer.
	getline(configurations, confLine);
	confLine = confLine.substr(confLine.find("=") + 2);
	uint consolePriority = atoi(confLine.c_str()); //convert console priority given as string to an integer.
	LoggerParams loggerParams(logFileName,filePriority,consolePriority);
	CAppLogger::InitializeLoggerParams(loggerParams);
	Simulation * sim = new Simulation(argv[2],argv[3],argv[4]); // 2 = products file. 3 = suppliers file. 4 = events file.
	delete sim;
	return 0;
}


