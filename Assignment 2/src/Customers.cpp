#include "../include/Customers.h"
#include <vector>
using namespace std;


Customers::Customers():m_customers(){}
vector<Customer *>& Customers::getCustomers()
{
	return m_customers;
}
Customers::~Customers()
{
	for (size_t i = 0; i<m_customers.size(); i++)
	{
		delete (m_customers.at(i));
	}
}
