/*
 * Menu.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: alexstol
 */
#include "../include/Menu.h"
#include <string.h>
#include <stdio.h>
#include<iostream>

Menu::Menu()
{
	inProducts=new vector<Product*>();
	exProducts=new vector<Product*>();
	allIngredients = new vector<Ingredient*>();
}

/*
 * constructor.
 */
/*
 *generates the first menu at start of simulation.
 */
void Menu::generateMenu(vector <vector<string> > prodVec, vector <vector<string> > suppVec){
	double price;
	int row=0;
	allIngredients = new vector<Ingredient*>();
	for (size_t i = 0;i < suppVec.at(0).size(); i++){
		cout << suppVec.at(0).at(i)<<endl;
		cout << "test1"<<endl;
		double pricei=atof(suppVec.at(i+1).at(1).c_str());//the price of the i ingredient
		allIngredients->push_back(new Ingredient(suppVec.at(0).at(i),suppVec.at(i+1).at(0),pricei));
	}

	for (size_t col = 0; col < prodVec.size(); col++){ // runs on each column in prodVec.
		price = 0;
		string product = prodVec.at(col).at(0); // puts the product of the current column in product.
		for (size_t rowi = 1; rowi < prodVec.at(col).size(); rowi++){ //runs on each row inside the current column, starting from row 1 to the end.(ingredients)
			price = price + findIngredientPrice((prodVec.at(col)).at(rowi), suppVec); // sets price to be = current price + price of added ingredient.
		}
		if (((price + 0.25)*1.5) <= 5 && strcmp(product.c_str(),(""))){
			Product* p=new Product(product);
			p->updatePrice(((price + 0.25)*1.5));
			for (size_t countOfIngreds = 1;countOfIngreds < prodVec.at(row).size(); countOfIngreds++){
				double currentIngredientPrice = findIngredientPrice(prodVec.at(row).at(countOfIngreds), suppVec); // finds lowest price for current ingredient.
				string currentSupplier = findIngredientSupplier(prodVec.at(col).at(countOfIngreds), suppVec, currentIngredientPrice); //finds the cheapest supplier of the current ingridient
				p->getIngredients().push_back(new Ingredient(prodVec.at(row).at(countOfIngreds),currentSupplier,currentIngredientPrice));
				//the arguments for Ingredient were copied from previous line(29.30).
			}
			inProducts->push_back(p);
		}
		else{// push into exproducts vector
			Product* p=new Product(product);
			p->updatePrice(((price + 0.25)*1.5));
			for (size_t countOfIngreds = 1;countOfIngreds < prodVec.at(row).size(); countOfIngreds++){
				double currentIngredientPrice = findIngredientPrice(prodVec.at(row).at(countOfIngreds), suppVec); // finds lowest price for current ingredient.
				string currentSupplier = findIngredientSupplier(prodVec.at(col).at(countOfIngreds), suppVec, currentIngredientPrice); //finds the cheapest supplier of the current ingridient
				p->getIngredients().push_back(new Ingredient(prodVec.at(row).at(countOfIngreds),currentSupplier,currentIngredientPrice));
			}
			exProducts->push_back(p);
		}
		row++;
	}
}

/*
 * find cheapest price of ingredient.
 */
double Menu::findIngredientPrice(string ingredient,vector <vector<string> > suppVec){
	double price = 10;
	for (size_t i = 0; i < suppVec.at(0).size(); i++ ){ // runs on all the ingredient
		if (atof((suppVec.at(i+1).at(1)).c_str()) < price && strcmp(ingredient.c_str(),suppVec.at(0).at(i).c_str())==0)
			price = atof((suppVec.at(i+1).at(1)).c_str());
	}
	return price;
}
double Menu::findIngredientPrice(string ingredient,vector <Ingredient* >& suppVec,double newPrice,string supplier){//finds the minimum price of the ingredient
	for (size_t i = 0; i < suppVec.size(); i++ ){
		if(strcmp(ingredient.c_str(),suppVec.at(i)->getIngredient().c_str())==0 && (strcmp(supplier.c_str(),suppVec.at(i)->getSupplier().c_str())==0))
			{
			suppVec.at(i)->setPrice(newPrice);
			}

	}
	double price = 10;
	for (size_t i = 0; i < suppVec.size(); i++ ){ // runs on all the ingredient
		if (suppVec.at(i)->getPrice() < price && strcmp(ingredient.c_str(),suppVec.at(i)->getIngredient().c_str())==0){
			price = suppVec.at(i)->getPrice();
		}
	}
	return price;
}
/*
 * finds the cheapest supplier of the current ingredient
 */
string Menu::findIngredientSupplier(string ingredient, vector<vector<string> > suppVec, double price){    //finds supplier of the ingredient's cheapest price.
	string supplier;
	bool found = false;
	for (size_t i = 0; i < suppVec.at(0).size()&&!found; i++){
		if (strcmp(ingredient.c_str(), suppVec.at(0).at(i).c_str())==0){
			if (price == atof((suppVec.at(i+1).at(1)).c_str())){
				supplier = suppVec.at(i+1).at(0);
				found = true;
			}
		}
	}
	return supplier;
}
string Menu::findIngredientSupplier(string ingredient, vector <Ingredient* >& suppVec, double price){    //finds supplier of the ingredient's cheapest price.
	string supplier;
	bool found = false;
	for (size_t i = 0; i < suppVec.size()&&!found; i++){
		if (strcmp(ingredient.c_str(), suppVec.at(i)->getIngredient().c_str())==0){
			if (price == suppVec.at(i)->getPrice()){
				supplier = suppVec.at(i)->getSupplier();
				found = true;
			}
		}
	}
	return supplier;
}
/*
 * returns price of a product.
 */
double Menu::FindProductPrice(string product){

	for(size_t i=0;i<inProducts->size();i++){
		if(strcmp((inProducts->at(i)->getProduct()).c_str(),product.c_str())==0)return (inProducts->at(i)->getPrice());
	}
	return -1;
}

/*
 * returns vector of products included in the menu.
 */
vector<Product*>* Menu:: getInProducts(){
	return inProducts;
}

/*
 * fixes menu after event of change price of ingredient.
 */
int Menu::updateMenu(string supplier,string ingredient,double newPrice){
	int c=0;//counter of products that include the changed ingredient.
	for(size_t i = 0 ; i < inProducts->size() ; i++){
		for(size_t j=0;j<inProducts->at(i)->getIngredients().size();j++){
			if(strcmp(ingredient.c_str(),inProducts->at(i)->getIngredients().at(j)->getIngredient().c_str())==0){
				double oldIngPrice = inProducts->at(i)->getIngredients().at(j)->getPrice();
				if(newPrice>oldIngPrice && strcmp(supplier.c_str(),inProducts->at(i)->getIngredients().at(j)->getSupplier().c_str())==0){
					inProducts->at(i)->getIngredients().at(j)->setPrice(newPrice);
					double lowestPrice=findIngredientPrice(ingredient,*allIngredients,newPrice,supplier);
					string suppmin=findIngredientSupplier(ingredient,*allIngredients,lowestPrice);
					inProducts->at(i)->getIngredients().at(j)->setSupplier(suppmin);
					inProducts->at(i)->getIngredients().at(j)->setPrice(lowestPrice);
					inProducts->at(i)->updatePrice((((((inProducts->at(i)->getPrice()/1.5)-0.25)-oldIngPrice+lowestPrice)+0.25)*1.5));
					c++;
					inProducts->at(i)->getIngredients().at(j)->setSupplier(supplier);
					if(inProducts->at(i)->getPrice()>5){
						CAppLogger::Instance().Log("Product "  + inProducts->at(i)->getProduct() + " was removed from the menu.",Poco::Message::PRIO_WARNING);
						inProducts->at(i)->getIngredients().at(j)->setPrice(newPrice);
						exProducts->push_back(inProducts->at(i));//push back to exProduct vector
						inProducts->erase(inProducts->begin()+i);//
					}
				}
				else {
					if(newPrice<oldIngPrice){
						inProducts->at(i)->getIngredients().at(j)->setSupplier(supplier);
						inProducts->at(i)->getIngredients().at(j)->setPrice(newPrice);
						inProducts->at(i)->updatePrice((((((inProducts->at(i)->getPrice()/1.5)-0.25)-oldIngPrice+newPrice)+0.25)*1.5));
						c++;
						if(inProducts->at(i)->getPrice()>5)
						{
							CAppLogger::Instance().Log("Product "  + inProducts->at(i)->getProduct() + " was removed from the menu.",Poco::Message::PRIO_WARNING);
							inProducts->at(i)->getIngredients().at(j)->setPrice(newPrice);
							exProducts->push_back(inProducts->at(i));//push back to exProduct vector
							inProducts->erase(inProducts->begin()+i);//
						}
					}
				}
			}
		}
	}
	for(size_t i=0;i<exProducts->size();i++){
		for(size_t j=0;j<exProducts->at(i)->getIngredients().size();j++){
			if((strcmp(ingredient.c_str(),exProducts->at(i)->getIngredients().at(j)->getIngredient().c_str())==0)){
				double oldIngPrice = exProducts->at(i)->getIngredients().at(j)->getPrice();
				if(newPrice>oldIngPrice && strcmp(supplier.c_str(),exProducts->at(i)->getIngredients().at(j)->getSupplier().c_str())==0){//the new price smaller than the old price
					exProducts->at(i)->getIngredients().at(j)->setSupplier(supplier);
					exProducts->at(i)->getIngredients().at(j)->setPrice(newPrice);
					double lowestPrice=findIngredientPrice(ingredient,*allIngredients,oldIngPrice,supplier);
					exProducts->at(i)->updatePrice((((((inProducts->at(i)->getPrice()/1.5)-0.25)-oldIngPrice+lowestPrice)+0.25)*1.5));
					if(exProducts->at(i)->getPrice()<5){
						CAppLogger::Instance().Log("Product "  + exProducts->at(i)->getProduct() + " was added from the menu.",Poco::Message::PRIO_WARNING);
						exProducts->at(i)->getIngredients().at(j)->setPrice(newPrice);
						inProducts->push_back(exProducts->at(i));//push back to exProduct vector
						exProducts->erase(exProducts->begin()+i);//////push back to inProduct vector
					}
				}
				else
				{
					if(newPrice<oldIngPrice)//(1)
					{
						exProducts->at(i)->getIngredients().at(j)->setSupplier(supplier);
						exProducts->at(i)->getIngredients().at(j)->setPrice(newPrice);
						exProducts->at(i)->updatePrice((((((inProducts->at(i)->getPrice()/1.5)-0.25)-oldIngPrice+newPrice)+0.25)*1.5));
						if(exProducts->at(i)->getPrice()<5){
							CAppLogger::Instance().Log("Product "  + exProducts->at(i)->getProduct() + " was added from the menu.",Poco::Message::PRIO_WARNING);
							inProducts->push_back(exProducts->at(i));//push back to exProduct vector
							//cout << inProducts->at(inProducts->size()-1)->getProduct()<<"  test11"<<endl;
							exProducts->erase(exProducts->begin()+i);//////push back to inProduct vector
						}
					}
				}

			}
		}
	}
	return c;
}

/*
 * returns vector of products excluded from the menu.
 */
vector<Product*> * Menu::getExProducts(){
	return exProducts;
}

/*
 * destructor.
 */
Menu::~Menu()
{
	inProducts->erase(inProducts->begin(),inProducts->end());
	exProducts->erase(exProducts->begin(),exProducts->end());
	allIngredients->erase(allIngredients->begin(),allIngredients->end());
}




