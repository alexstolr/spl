/*
 * Ingredient.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: alexstol
 */
#include "../include/Ingredient.h"
#include <iostream>

Ingredient::Ingredient(string ingredient, string supplier, double price){
	_ingredient=ingredient;
	_supplier=supplier;
	_price=price;
}
double Ingredient::getPrice(){
	return _price;
}
string Ingredient::getIngredient(){
	return _ingredient;
}
string Ingredient::getSupplier(){
	return _supplier;
}
void Ingredient::setPrice(const double p){
	_price=p;
}
void Ingredient:: setSupplier(const string s)
{
	_supplier=s;
	}


Ingredient::~Ingredient()
{

}


