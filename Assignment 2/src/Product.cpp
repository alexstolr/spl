/*
 * Product.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: alexstol
 */
#include "../include/Product.h"


Product::Product(string product){
	this->product=product;
	price=0;
	vector<Ingredient *> *  ingredients = new vector<Ingredient *>();
}

double Product:: getPrice(){
	return price;
}
void Product::updatePrice(double p)
{
price=p;
}

void Product::addToPrice(double p){
	price = price + p;
}
string Product::getProduct(){
	return product;
}
vector<Ingredient *>& Product:: getIngredients(){
	return Ingredients;
}

Product::~Product(){
	for (size_t i = 0;  i < Ingredients.size(); i++) {
		delete(Ingredients.at(i));
		Ingredients.at(i)=0;
	}
}


