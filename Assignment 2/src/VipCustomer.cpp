#include "../include/VipCustomer.h"
VipCustomer::VipCustomer(const string name, const string product,const  string img):_name(name),_favProduct(product),_img(img){
}
double VipCustomer::computeProductPrice(double originalPrice){
	return originalPrice*0.8;
}
string VipCustomer::getName() const{
	return _name;
}
string VipCustomer::getFavouriteProduct() const{
	return _favProduct;
}
string VipCustomer::getImg()
{
	return _img;
}
VipCustomer::~VipCustomer(){}


