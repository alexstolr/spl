#include "../include/RegularCustomer.h"


RegularCustomer::RegularCustomer(const string name,const  string product,const  string img):_name(name),_favProduct(product),_img(img){

}
double RegularCustomer::computeProductPrice(double originalPrice){
	return originalPrice;
}
		string RegularCustomer::getName() const{
			return _name;
		}
		string RegularCustomer::getFavouriteProduct() const{
			return _favProduct;
		}
		string RegularCustomer::getImg()
		{
		return _img;
		}
		 RegularCustomer::~RegularCustomer()
		{
		}
