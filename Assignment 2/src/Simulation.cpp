/*
 * Functions.cpp

 *
 *  Created on: Nov 18, 2013
 *      Author: alexstol
 */
#include "../include/Simulation.h"
#include "../include/ImageOperations.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

using namespace cv;
using namespace Poco;
/*
 *builder of simulation.
 *also generates initial menu.
 */
Simulation::Simulation(char* productsFile, char* suppliersFile, char* eventsFile):menu(),prodVec(),suppVec(),profit(0),revenue(0),prodLine(),suppLine(),newCustomer()
	{
	menu=new Menu();
	newCustomer=new Customers();
	fstream products;
	products.open(productsFile);
	fstream suppliers;
	suppliers.open(suppliersFile);
	fstream events;
	events.open(eventsFile);

	//the following code produces the vectors for products and suppliers similar to Assignment 1.
	// products vector will look like this : product,ingredient 1,ingredient 2,...,ingredient n,  for each product
	//suppliers vector will look like this :
	//product 1,product 2,......,product n
	//supplier of product1, price of product1 given by this supplier
	//supplier of product2, price of product2 given by this supplier
	//   .           .                  .
	if (products.is_open()){
		while (!products.eof()){  // while not finished going thru the whole file.
			getline(products, prodLine);  // put first line from Products.conf to prodLine.
			vector<string> newVec; // this vector will contain the product in the first place and its ingredients in the next cells.
			prodLine = prodLine + ',';
			while (prodLine.length() - 1 != prodLine.find(',')){ //when (prodLine.length() - 1)  = ',' --> reached end of the line.
				newVec.push_back(prodLine.substr(0, prodLine.find(','))); //push : product,ingredient 1,ingredient 2,...,ingredient n,  for each product
				prodLine = prodLine.substr(prodLine.find(',') + 1);
			}
			newVec.push_back(prodLine.substr(0, prodLine.find(',')));
			prodLine = prodLine.substr(prodLine.find(',') + 1);
			prodVec.push_back(newVec); //pushes each newVec into prodVec.
			prodLine = "";
		}

		products.close(); // closes for reading of products
	}
	int row = 1;
	vector<string> row1;
	suppVec.push_back(row1);
	if (suppliers.is_open()){
		while (!suppliers.eof()){
			getline(suppliers, suppLine);
			vector<string> rowi;
			suppVec.push_back(rowi);
			suppVec.at(row).push_back(suppLine.substr(0, suppLine.find(',')));
			suppLine = suppLine.substr(suppLine.find(',') + 1);  //prodLine doesnt include the product.
			suppVec.at(0).push_back(suppLine.substr(0, suppLine.find(',')));
			suppLine = suppLine.substr(suppLine.find(',') + 1);
			suppVec.at(row).push_back(suppLine.substr(0, suppLine.find(',')));
			row++;
		}
		suppliers.close();
	}
	string eventLine;
	cout << "test"<<endl;
	menu->generateMenu(prodVec,suppVec);
	cout << "test"<<endl;
	for(size_t i = 0 ; i < menu->getInProducts()->size() ; i++){
		cout << menu->getInProducts()->at(i)->getProduct() << menu->getInProducts()->at(i)->getPrice();
		for(size_t j = 0 ; j < menu->getInProducts()->at(i)->getIngredients().size() ; j++){
			cout <<","<< menu->getInProducts()->at(i)->getIngredients().at(j)->getIngredient() << "##"<<menu->getInProducts()->at(i)->getIngredients().at(j)->getSupplier();
		}
		cout<< endl;
	}
	cout<< endl;
		for(size_t i = 0 ; i < menu->getExProducts()->size() ; i++){
			cout << menu->getExProducts()->at(i)->getProduct() << menu->getExProducts()->at(i)->getPrice();
			for(size_t j = 0 ; j < menu->getExProducts()->at(i)->getIngredients().size() ; j++){
				cout <<","<< menu->getExProducts()->at(i)->getIngredients().at(j)->getIngredient() << "##"<<menu->getExProducts()->at(i)->getIngredients().at(j)->getSupplier();
			}
			cout<< endl;
		}
	while(!events.eof())
	{
		getline(events,eventLine);
		//cout<<eventLine<<endl;
		readNextEvent(eventLine);
	}
	endSimulation();
}
void Simulation::readNextEvent(string eventLine){
	string event = eventLine.substr(0,eventLine.find(','));
	//cout<<event<<endl;
	eventLine=eventLine.substr(eventLine.find(',') + 1);
	if(strcmp(event.c_str(),"register") == 0){
		registerEvent(eventLine);
	}
	else if(strcmp(event.c_str(),"purchase") == 0)
		purchaseEvent(eventLine);
	else {
		suppUpdateEvent(eventLine);
	}

}

void Simulation::registerEvent(string registerLine){
	const string name = registerLine.substr(0,registerLine.find(','));
	const string img=name+".tiff";
	registerLine=registerLine.substr(registerLine.find(',')+1);
	const string product =registerLine.substr(0,registerLine.find(','));
	registerLine=registerLine.substr(registerLine.find(',')+1);
	string isVip=registerLine.substr(0,registerLine.find(','));
	if(strcmp(isVip.c_str(),"1")==0){
		Customer* c= new RegularCustomer(name,product,img);
		newCustomer->getCustomers().push_back(c);
		CAppLogger::Instance().Log("new Vip Customer registered - "+name+" Favorite Product - "+product,Poco::Message::PRIO_NOTICE);
	}
	else {Customer* c=new VipCustomer(name,product,img);
	newCustomer->getCustomers().push_back(c);
	CAppLogger::Instance().Log("new Regular Customer registered - "+name+" Favorite Product - "+product,Poco::Message::PRIO_NOTICE);
	}
}

void Simulation::purchaseEvent(string purchaseLine){
	purchaseLine="customers/"+purchaseLine; //  "customer/customer_name.tiff"
	Mat objectImg(imread(purchaseLine)); //objectImg is the received picture.
	Mat objectImageGray(objectImg.rows, objectImg.cols, CV_8UC3); // defines image gray of wanted parameters.
	cvtColor(objectImg,objectImageGray,CV_BGR2GRAY);
	/*cvtColor(m_image1, objectImageGray,CV_BGR2GRAY); // turns m_image1 into gray
	//create image window named "My image"
	   namedWindow("My image");
	        // show the image on window
	        imshow("My image", objectImageGray);
	        // wait key for 5000 ms
	        waitKey(5000);*/
	string face_cascade_name = "/usr/share/opencv/haarcascades/haarcascade_frontalface_alt.xml"; // "user/share/opencv/haarcascades/file_name.xml"
	CascadeClassifier face_cascade;
	face_cascade.load(face_cascade_name); // use the model to detect faces in the image.
	vector<Rect> faces; // faces is the vector of frames of faces.
	face_cascade.detectMultiScale(objectImageGray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE); // finds the face frame and puts it into face_cascade.
	//cout<< faces.size()<<endl;
	for(size_t i=0;i<faces.size();i++){
		Mat toCompare=Mat(objectImageGray,faces.at(i)); //toCopmare is the i'th face from the faces as a gray image
		//create image window named "My image"
		for(size_t j=0;j<newCustomer->getCustomers().size();j++){
			string customerName=newCustomer->getCustomers().at(j)->getName();
			Mat img(imread("faces/"+customerName+"/"+customerName+".tiff"));
			Mat imgGray(toCompare.rows,toCompare.cols,CV_8UC3);
			cvtColor(img, imgGray,CV_BGR2GRAY);
			Mat result;
			if((toCompare.rows==imgGray.rows)&&(toCompare.cols==imgGray.cols)){
			compare(toCompare,imgGray , result ,CMP_NE);
			int similairPixels = countNonZero(result);
			//cout << similairPixels<<" pixels"<<endl;
			if(similairPixels == 0){
				//cout << "test 5"<<endl;
				double price=menu->FindProductPrice(newCustomer->getCustomers().at(j)->getFavouriteProduct());
				double price1=newCustomer->getCustomers().at(j)->computeProductPrice(price);
				if(price1>0){
					CAppLogger::Instance().Log("Costumer " + newCustomer->getCustomers().at(j)->getName() + " purchased " + (newCustomer->getCustomers().at(j)->getFavouriteProduct()) ,Poco::Message::PRIO_WARNING);
					revenue=revenue+price1;
					profit=profit+(price1-((price)/1.5)-0.25);
				}
				else CAppLogger::Instance().Log("Costumer " + newCustomer->getCustomers().at(j)->getName() + " failed to purchase " + (newCustomer->getCustomers().at(j)->getFavouriteProduct()) ,Poco::Message::PRIO_WARNING);
			}
			}

		}
	}
}

void Simulation::suppUpdateEvent(string suppUpdateEvent){
	string supp=suppUpdateEvent.substr(0,suppUpdateEvent.find(","));//takes the suppiler
	suppUpdateEvent=suppUpdateEvent.substr(suppUpdateEvent.find(",")+1);
	string ingredient=suppUpdateEvent.substr(0,suppUpdateEvent.find(","));
	suppUpdateEvent=suppUpdateEvent.substr(suppUpdateEvent.find(",")+1);
	double price=atof(suppUpdateEvent.c_str()); //converts price to double
	int c=menu->updateMenu(supp,ingredient,price);
	string s;
	ostringstream convert;   // stream used for the conversion
	convert << c;      // insert the textual representation of 'Number' in the characters in the stream
	s = convert.str(); // set 'Result' to the contents of the stream
	CAppLogger::Instance().Log("Supplier " +supp+ " changed the price of " +ingredient,Poco::Message::PRIO_NOTICE);
	CAppLogger::Instance().Log("Products updated: " + s,Poco::Message::PRIO_NOTICE);
}
/**
 * generates collage of faces.
 */
void Simulation::endSimulation(){
	ImageOperations op;
	Mat collage(105,newCustomer->getCustomers().size()*105 , CV_8UC3);//defines a frame size of 105*(number of registered customers)
	for(size_t i = 0 ; i<newCustomer->getCustomers().size() ; i++){
		Mat face(105,105,CV_8UC3); // define new frame.
		string customer = newCustomer->getCustomers().at(i)->getName(); // customer name.
		Mat face1(imread("faces/" + customer + "/"+customer + ".tiff"));
		op.resize(face1,face);
		op.copy_paste_image(face,collage,105*i);
	}
	imwrite("collage.tiff",collage);
	//logger message revenue and profit
	string revenue1;
	ostringstream convert;   // stream used for the conversion
	convert << revenue;      // insert the textual representation of 'Number' in the characters in the stream
	revenue1 = convert.str(); // set 'Result' to the contents of the stream
	string profit1;
	convert << profit;      // insert the textual representation of 'Number' in the characters in the stream
	profit1 = convert.str(); // set 'Result' to the contents of the stream
	CAppLogger::Instance().Log("The total revenue is " + revenue1 + ", while the total profit is " + profit1,Poco::Message::PRIO_WARNING);
}


Simulation::~Simulation(){
	delete newCustomer;
	delete menu;
}


