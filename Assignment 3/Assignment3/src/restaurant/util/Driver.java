package restaurant.util;

import org.jdom2.Document;


import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.SAXException;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Driver class functions as the main which executes the whole program - main thread. 
 * @author ALEX & Daniel
 *
 */
public class Driver {

	private static final int INITIAL_DATA_FILE = 1;
	private static final int MENU_FILE = 2;
	private static final int ORDER_LIST_FILE = 3;
	final static Logger LOGGER = Logger.getLogger(Driver.class .getName());

	/**
	 * main
	 * @param argv input files
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws JDOMException 
	 */
	public static void main(String argv[]) throws ParserConfigurationException, SAXException, IOException, JDOMException {
		LOGGER.setLevel(Level.INFO); //set logger to info.
		LinkedBlockingQueue<Order> ordersToDeliver = new LinkedBlockingQueue<Order>();
		Semaphore activateChef = new Semaphore(0); //used for initialization.
		Management management = new Management();
		FileHandler fileTxt = new FileHandler("Logging.txt");//Logger output text initialize
		SimpleFormatter formatterTxt = new SimpleFormatter();
		fileTxt.setFormatter(formatterTxt);
		LOGGER.addHandler(fileTxt);
		SAXBuilder builder = new SAXBuilder();
		handleDocumentParser(management, argv[0], builder, INITIAL_DATA_FILE, ordersToDeliver);//handle initialData
		handleDocumentParser(management, argv[1], builder, MENU_FILE, ordersToDeliver);//handle menu
		handleDocumentParser(management, argv[2], builder, ORDER_LIST_FILE, ordersToDeliver);//handle orderList
		LOGGER.info("Initializing simulation process...");
		CountDownLatch shutDownChef = new CountDownLatch(management.getChefsCount());
		management.PostParserInitialize(activateChef,shutDownChef,ordersToDeliver);
		management.sendOrdersToCook(); // sends the orders to be cooked.
		LOGGER.info("Statistics: " + management.getStatistics());
	}

	/**
	 * decides to which parses to send the file, depending on its name.
	 */
	private static void handleDocumentParser(Management management,String fileXml, SAXBuilder builder,int argument, LinkedBlockingQueue<Order> ordersToDeliver) throws SAXException,IOException, JDOMException{
		Document doc = builder.build(fileXml);
		verifyFileExists(fileXml);
		if(argument == INITIAL_DATA_FILE)
			InitialDataHandler.parseInitialDataXml(management, doc, ordersToDeliver);
		else if(argument == MENU_FILE)
			MenuHandler.parseMenuXml(management, doc);
		else if(argument == ORDER_LIST_FILE)
			OrderListHandler.parseOrderListXml(management, doc);
	}

	/**
	 * verifies the file exists.
	 * @param fileName file name
	 * @throws FileNotFoundException
	 */
	private static void verifyFileExists(String fileName) throws FileNotFoundException {
		File file = new File(fileName);
		if(!file.exists()){
			throw new FileNotFoundException(fileName);
		}
	}

}
