package restaurant.util;
import java.util.Vector;

/**
 * represents a menu.
 * @author Daniel & ALEX
 *defines the menu.
 */
public class Menu {
	private Vector<Dish> _menuVec;

	public Menu(){
		_menuVec = new Vector<Dish>();
	}

	/**
	 * adds a dish to the menu vector.
	 * @param dish a dish
	 */
	public void addDish(Dish dish){
		_menuVec.addElement(dish);
	}

	/**
	 * returns a dish from the dishes of the menu.
	 * @param dish
	 * @return a dish
	 */
	public Dish getDish(Dish dish){
		return _menuVec.elementAt(_menuVec.indexOf(dish));
	}

	/**
	 * returns a dish
	 * @param dish name of dish
	 * @return Dish
	 */
	public Dish getDish(String dish){
		for (int i = 0; i < _menuVec.size(); i++) {
			if(dish.equals(_menuVec.get(i).get_name())){
				return  _menuVec.get(i);
			}
		}
		return null;

	}
}
