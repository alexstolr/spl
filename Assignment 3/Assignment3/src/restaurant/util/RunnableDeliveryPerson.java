package restaurant.util;
import java.util.Calendar;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * simulates a delivery made by a delivery person
 * @author Daniel & ALEX
 *
 */
public class RunnableDeliveryPerson implements Runnable {

	private final static int DELIVERED = 4;

	private String name;
	private Address address;
	private long speed;
	private CountDownLatch delPerDone;
	private LinkedBlockingQueue<Order> ordersToDeliver;
	private boolean shouldStop = false;
	private Statistics statistics;

	/**
	 * 
	 * @param name name of delivery person.
	 * @param address address of restaurant.
	 * @param speed speed of delivery person.
	 * @param ordersToDeliver Array of orders that need to be delivered.
	 */
	public RunnableDeliveryPerson(String name, Address address, long speed, LinkedBlockingQueue<Order> ordersToDeliver, Statistics statistics) {
		this.name = name;
		this.address=new Address(address);
		this.speed = speed;
		this.ordersToDeliver = ordersToDeliver;
		this.statistics = statistics;
	}

	/**
	 * method used to terminate thread.
	 */
	public void terminate() {
		shouldStop = true;
	}

	/**
	 * 
	 * @param address address of client to deliver.
	 * @return expected delivery time to the clients house.
	 */
	public int expectedDeliveryTime(Address address){
		return (int)((this.address.calculateDistance(address))/speed);
	}

	/**
	 * implement run
	 */
	@Override
	public void run(){
		while(!shouldStop){
			try{
				Order order = ordersToDeliver.take();
				if(!(order.getOrderId().equals("-1"))){	//if not fake order
					long expectedDeliveryTime=expectedDeliveryTime(order.getCustomerAddress());
					long dateBefore=Calendar.getInstance().getTimeInMillis();// data before delivery
					Thread.sleep(expectedDeliveryTime);
					long dateAfter = Calendar.getInstance().getTimeInMillis();// the time after the cooking
					long realTimeDelivery=dateAfter-dateBefore;
					if((realTimeDelivery+order.getRealTimeCook()) >= 1.15*(order.getExpectedCookTime()+expectedDeliveryTime)){
						statistics.set_balance(order.getReward()*0.5,Integer.parseInt(order.getOrderId()),order.getReward());
					}
					else{
						statistics.set_balance(order.getReward(),Integer.parseInt(order.getOrderId()),order.getReward());
					}
					statistics.addDeliveredOrders(order);
					Thread.sleep(expectedDeliveryTime(order.getCustomerAddress()));
					order.setOrderStatus(DELIVERED);
					delPerDone.countDown();
					Driver.LOGGER.info("Order DELIVERED: [id="+order.getOrderId()+"][expectedTimeToDeliver= "+expectedDeliveryTime+"][realTimeSpentToDeliver= "+realTimeDelivery+" ]");
				}
			}
			catch (InterruptedException e) {
				System.out.println("error " + name);
			} catch (Exception e) {
				System.out.println("ERROR!" + e);
				e.printStackTrace();
			}
		}

	}

	public void initializeCountDownLatch(CountDownLatch delPerDone) {
		this.delPerDone = delPerDone;
	}
}

