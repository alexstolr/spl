package restaurant.util;
import java.util.Calendar;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

/**
 * cooks a whole order.
 * @author ALEX & Daniel
 */
public class CallableCookWholeOrder implements Callable<Order>{
	private final static int COMPLETE = 3;

	private Order order;
	private Statistics statistics;
	private RunnableChef chef;
	private Warehouse warehouse;
	private Semaphore activateChef;
	private CountDownLatch dishesLeft;

	/**
	 * constructor
	 * @param order
	 * @param statistics
	 * @param chef
	 * @param warehouse
	 * @param activateChef 
	 */
	public CallableCookWholeOrder(Order order, Statistics statistics,	RunnableChef chef, Warehouse warehouse, Semaphore activateChef) {
		this.order = order;
		this.statistics = statistics;
		this.chef = chef;
		this.warehouse = warehouse;
		this.activateChef = activateChef;
		intilizeTheLeftDish();
		Driver.LOGGER.info("[CallableCookWholeOrder][orderId="+order.getOrderId()+"][numberOfMeals="+order.numOfMeals()+"]");
	}

	/**
	 * defines the process of running a whole order.
	 */
	@Override
	public Order call() throws Exception {
		Driver.LOGGER.info("INFO: [CallableCookWholeOrder] waiting for " + order.numOfMeals() + " meals to finish simulating...");
		long dateBefore=Calendar.getInstance().getTimeInMillis();//the time before the cooking
		for (int i = 0; i < order.OrderOfDishSize(); i++) {	//runs on all of dishes in the order
			for (int j = 0; j < order.getOrderOfDish(i).getQuantity(); j++) { //runs on the quantity cooked of each dish in the order.
				Thread cookOneDish=new Thread(new RunnableCookOneDish(this.chef,this.statistics,order.getOrderOfDish(i).get_dish(),this.warehouse,this.dishesLeft));
				cookOneDish.start(); // starts cooking current dish.
			}
		}
		dishesLeft.await(); //waits here until all dishes are cooked!
		long dateAfter = Calendar.getInstance().getTimeInMillis();// the time after the cooking
		long realTimeCook=dateAfter-dateBefore;
		Driver.LOGGER.info("[CallableCookWholeOrder]"+chef.get_name()+"  All meals have been cooked!");
		order.setRealTimeCook(realTimeCook);
		order.setOrderStatus(COMPLETE);		
		chef.set_currentPressure((-1)*order.getDifficultyRating()); // Decreases pressure.
		activateChef.release();//chef is available for more orders.
		Driver.LOGGER.info("Order COMPLETE:" +order.getOrderId());
		return order;
	}
	
	/**
	 * Initialize the CountDownLatch with the number of the dish in the order as a param.
	 */
	public void intilizeTheLeftDish(){
		dishesLeft=new CountDownLatch(order.numOfMeals());
	}

}
