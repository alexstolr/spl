package restaurant.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.jdom2.Document;
import org.jdom2.Element;
/**
 * handles parsing of Menu.xml
 * @author ALEX & Daniel
 *
 */
public class MenuHandler {

	Document menuDoc;
	/**
	 * constructor
	 * @param menuDoc
	 */
	public MenuHandler(Document menuDoc) {
		this.menuDoc = menuDoc;
	}
	
	/**
	 * parses menu element
	 * @param management
	 * @param menuDoc
	 */
	public static void parseMenuXml(Management management, Document menuDoc){
		Element menu = menuDoc.getRootElement();
		handleDishesParsing(menu,management.getMenu());
	}
	
	/**
	 * handles parsing dishes element
	 * @param menu
	 */
	public static void handleDishesParsing(Element menu,Menu manu){
		Element dishes = menu.getChild("Dishes");
		handleDishParsing(dishes,manu);
	}
	
	/**
	 * handles parsing of dish element
	 * @param dishes
	 */
	public static Menu handleDishParsing(Element dishes,Menu menu){
		List<Element> dish = dishes.getChildren();
		for (int i = 0; i < dish.size(); i++) {
			Element currDish = dish.get(i);
			Element name = currDish.getChild("name");
			Element difficultyRating = currDish.getChild("difficultyRating");
			Element expectedCookTime = currDish.getChild("expectedCookTime");
			Element reward = currDish.getChild("reward");
			menu.addDish(new Dish(name.getText(), Integer.parseInt(expectedCookTime.getText()), Integer.parseInt(difficultyRating.getText()), Integer.parseInt(reward.getText()), handleIngredientsParsing(currDish), handleKitchenToolsParsing(currDish)));
		}
		return menu;
	}
	
	/**
	 * handles parsing of kitchentools element
	 * @param currDish
	 */
	public static Vector<KitchenTool> handleKitchenToolsParsing(Element currDish){
		Vector<KitchenTool> dishKTools = new Vector<KitchenTool>();
		Element kitchenTools = currDish.getChild("KitchenTools");
		List<Element> kitchenTool = kitchenTools.getChildren();	
		for (int i = 0; i < kitchenTool.size(); i++) {
			Element currKitchenTools = kitchenTool.get(i);
			Element name = currKitchenTools.getChild("name");
			Element quantity = currKitchenTools.getChild("quantity");
			dishKTools.add(new KitchenTool(name.getText(), Integer.parseInt(quantity.getText())));
		}
		return dishKTools;
	}
	
	/**
	 * handles parsing of Ingredients element
	 * @param currDish
	 */
	public static ArrayList<Ingredient> handleIngredientsParsing(Element currDish){
		ArrayList<Ingredient> dishIngredients = new ArrayList<Ingredient>();
		Element ingredients = currDish.getChild("Ingredients");
		List<Element> Ingredient = ingredients.getChildren();
		for (int i = 0; i < Ingredient.size(); i++) {
			Element currIngredient = Ingredient.get(i);
			Element name = currIngredient.getChild("name");
			Element quantity = currIngredient.getChild("quantity");
			dishIngredients.add(new Ingredient(name.getText(), Integer.parseInt(quantity.getText())));
		}
		return dishIngredients;
	}
	

}
