package restaurant.util;

import java.util.concurrent.CountDownLatch;
import java.lang.Math;

/**
 * implements the cooking of one dish
 * @author Daniel& ALEX
 *
 */
public class RunnableCookOneDish implements Runnable{
	private final static int IN_PROGRESS = 2;
	private final static int COMPLETE = 3;

	private RunnableChef chef;
	private Statistics statistics;
	private Dish dish;
	private Warehouse warehouse;
	private CountDownLatch dishIsDone;

	/**
	 * constructor
	 * @param chef
	 * @param statistics
	 * @param dish
	 * @param warehouse
	 * @param countDownLatch 
	 */
	public RunnableCookOneDish(RunnableChef chef, Statistics statistics, Dish dish, Warehouse warehouse, CountDownLatch countDownLatch) {
		this.chef = chef;
		this.statistics = statistics;
		this.dish = dish;
		this.warehouse = warehouse;
		this.dishIsDone =countDownLatch;
	}

	/**
	 * before the cooking starts, takes the need ingredients from the warehouse(and adds them to the consumed ingredients.)
	 * takes the kitchen tools needed to prepare the dish.
	 */
	protected void beforeTheCooking (){
		try {
			dish.setDishStatus(IN_PROGRESS);
		} catch (Exception e) {
			System.out.println("ERROR!! no such status." + e);
			e.printStackTrace();
		}
		for (int i = 0; i < dish.ingredientSize(); i++) {
			statistics.addConsumedIngreds(dish.getDishIngredient(i));
			warehouse.decreaseIngredientQuantity(dish.getDishIngredient(i).getQuantity(), dish.getDishIngredient(i).getName());
		}
			for (int i = 0; i < dish.kToolSize(); i++) {
				warehouse.getKTool(dish.getDishkTool(i).getName()).acquire(dish.getDishkTool(i).getQuantity());		
		}
	}

	/**
	 * returns all the kTools to the warehouse.
	 */
	protected void AfterTheCooking(){
		this.dishIsDone.countDown();	
		for (int i =0 ; i <dish.kToolSize() ; i++) {
			warehouse.getKTool(dish.getDishkTool(i).getName()).release(dish.getDishkTool(i).getQuantity());
		}
		try {
			dish.setDishStatus(COMPLETE);
		} catch (Exception e) {
			System.out.println(" ERROR! no such status" + e);
			e.printStackTrace();
		}
		Driver.LOGGER.info("the cooking of "+dish.get_name()+" is done! "+chef.get_name()+" "+dishIsDone.getCount());
	}

	/**
	 * calculates the expected cooking time of the current dish.
	 * @param efficancy
	 * @param difficulty
	 * @return the expected cooking time of the current dish
	 */
	public long theExpectedCookingTime(double efficancy,long difficulty){
		return (long)(Math.round(difficulty*efficancy));
	}

	@Override
	public void run() {
		this.beforeTheCooking(); 
		try {
			Thread.sleep(this.theExpectedCookingTime(chef.getEfficiency(),dish.get_difficultyRating())); // performs the cook of the current dish
		} catch (InterruptedException e) {
			System.out.println("ERROR!!: "+e);
			e.printStackTrace();
		}
		this.AfterTheCooking();
	}

}
