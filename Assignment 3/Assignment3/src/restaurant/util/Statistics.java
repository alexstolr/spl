 package restaurant.util;

import java.util.ArrayList;

public class Statistics {
	private ArrayList<Double> _balance;
	private ArrayList<Double> _originalReward;
	private ArrayList<Order> _deliveredOrders;
	private ArrayList<Ingredient> _consumedIngredients;
	public Statistics(){
		_balance =new ArrayList<Double>();	
		_deliveredOrders = new ArrayList<Order>();
		_consumedIngredients = new ArrayList<Ingredient>();
		_originalReward=new ArrayList<Double>();
	}
	public double get_balance(int i) {
		return _balance.get(i);
	}
	/**
	 * 
	 * @param _balance
	 * @param orderID
	 */
	public void set_balance(double _balance,int orderID,double oReward) {
		this._balance.add((double)orderID);
		this._balance.add(_balance);
		this._originalReward.add((double)orderID);
		this._originalReward.add(oReward);
	}
	/**
	 * add Delivered Order
	 * @param order
	 */
	public void addDeliveredOrders(Order order){
		_deliveredOrders.add(order);
	}

	/**
	 * adds the consumed ingredient to array list _consumedIngredients
	 * @param ingredient
	 */
	public void addConsumedIngreds(Ingredient ingredient){
		boolean flag= false;
		if(_consumedIngredients.size() > 0){
			for (int i = 0; i < _consumedIngredients.size() && !flag; i++) {
				if(_consumedIngredients.get(i).getName().equals(ingredient.getName())){
					_consumedIngredients.get(i).addQuantity(ingredient.getQuantity());
					flag = true;
				}
			}
			if(!flag) _consumedIngredients.add(new Ingredient(ingredient.getName(),ingredient.getQuantity()));
		}
		else _consumedIngredients.add(new Ingredient(ingredient.getName(),ingredient.getQuantity()));
	}
	public String Profit(){
		String sum="\n";
		for (int i = 0; i < _originalReward.size(); i++) {
			sum=sum+"["+_originalReward.get(i)+"]";
		}
		sum=sum+"\n";
		for (int i = 0; i < _balance.size(); i++) {
			sum=sum+"["+_balance.get(i)+"]";
		}
		return sum;
	}
	
	/**
	 * used to print all delivered orders in toString.
	 * @return
	 */
	public String deliveredOrdersTS(){
		String dOTS= "";
		for (int i = 0; i < _deliveredOrders.size(); i++) {
			dOTS = dOTS + " " + _deliveredOrders.get(i).getOrderId();
		}
		return dOTS;
	}
	
	public String consumedIngsTS(){
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < _consumedIngredients.size(); i++) {
			stringBuilder.append("[Name]"+_consumedIngredients.get(i).getName() +  ", [Quantity]"+_consumedIngredients.get(i).getQuantity() + " ;");
		}
		String dOTS = stringBuilder.toString();
		return dOTS;
	}
	@Override
	public String toString() {
		return "\n" + "Statistics: [start]" + "\n" + "\n" + "\t" + "[Balance]" + _balance +  "[Balance]" + "\n"  + "\t"  + "[Original Reward]"
				+ _originalReward + "[Original Reward]" + "\n" + "\n" + "\t" + "[delivered Orders][start]" + "\n" + _deliveredOrders
				+ "\n" + "\t" + "[delivered Orders][end]" + "\n" + "\n" + "\t" + "[consumedIngredients][start]" +
				"\n" + _consumedIngredients + "\n" + "\t" + "[consumedIngredients][end]" + "\n" + "\n" + "Statistics: [end]";
	}
	
	
	
	

}