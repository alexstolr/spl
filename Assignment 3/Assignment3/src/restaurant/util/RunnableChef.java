package restaurant.util;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.Vector;
public class RunnableChef  implements Runnable {

	private final static int IN_PROGRESS = 2;
	private final static int COMPLETE = 3;

	private boolean _shouldStop = false;
	private String _name;
	private double _efficiencyRating;
	private int _enduranceRating;
	private int _currentPressure;
	private ArrayList<Future<Order>> futuresForOrders;
	private Vector<Order> _ordersToCook;
	private Statistics _statistics;
	private ExecutorService executor;
	private Warehouse _warehouse;
	private LinkedBlockingQueue<Order> _ordersToDeliver;
	private Semaphore activateChef;
	private CountDownLatch shutDownChef;
	private Semaphore pressureDecrease;

	public RunnableChef(String name, double efficiencyRating, int enduranceRating, int currentPressure,Statistics statistics,Warehouse warehouse,
			LinkedBlockingQueue<Order> ordersToDeliver) {
		_name = name;
		futuresForOrders=new ArrayList<Future<Order>>();
		_efficiencyRating = efficiencyRating;
		_enduranceRating = enduranceRating;
		_currentPressure = currentPressure;
		_ordersToCook=new Vector<Order>();
		_warehouse=warehouse;
		executor=Executors.newCachedThreadPool();
		_ordersToDeliver = ordersToDeliver;
	}

	public synchronized void  Stop(){
		_shouldStop=true;
	}

	/**
	 * boolean used to cancel the thread.
	 * @return
	 */
	public synchronized boolean shouldStop() {
		return _shouldStop;
	} 


	public String get_name() {
		return _name;
	}

	public double get_efficiencyRating() {
		return _efficiencyRating;
	}

	public int get_enduranceRating() {
		return _enduranceRating;
	}

	/**
	 * changes chefs pressure after taking another order.
	 * @param currentPressure
	 */
	public synchronized void set_currentPressure(int difficultyRating) {
		_currentPressure =_currentPressure + difficultyRating;
		if(difficultyRating < 0){
			try{
				this.pressureDecrease.release();
			}catch(Exception e){
				System.out.println("Error :"+e);
			}
		}
	}
	public synchronized long get_currentPressure() {
		return _currentPressure;
	}
	/**
	 * try send an order to a chef. if he is able - will take and start cooking it.
	 * @param order
	 * @return boolean to let know that order was taken by a cheff.
	 */
	public void takeOrder(Order order){
		if(order.getDifficultyRating()<=this.get_enduranceRating()-this.get_currentPressure()){	//if difficulty not to high chef takes this order to prepare.
			try {
				order.setOrderStatus(IN_PROGRESS);
				Driver.LOGGER.info("Order ACCEPTED: "+  order.getOrderId());
				this.set_currentPressure(order.getDifficultyRating());
			} catch (Exception e) {
				e.printStackTrace();
			}
			this._ordersToCook.add(order);
			activateChef.release();
		}
		else{
			Driver.LOGGER.info("INFO: Order REFUSED: " + order.getOrderId() + " [difficulty="+ order.getDifficultyRating() +"][availableEndurance=" + (this.get_enduranceRating() - get_currentPressure()) + "]");

		}
	}

	/**
	 * run of RunnableChef. chef cooks his accepted order.
	 */
	@Override
	public void run() {
		while(!this.shouldStop() || !(this._ordersToCook.isEmpty()) || !(this.futuresForOrders.isEmpty())){
			try {
				activateChef.acquire();
			} catch (InterruptedException e1) {
				System.out.println("ERROR: " + e1);
				e1.printStackTrace();
			}
			for (int i = 0; i < _ordersToCook.size(); i++) { // runs on all order current chef has.
				Order order=_ordersToCook.get(i);
				_ordersToCook.remove(i);
				CallableCookWholeOrder cOrder= new CallableCookWholeOrder(order,this._statistics,this,this._warehouse,activateChef);
				Future<Order> fOrder = executor.submit(cOrder); //cooks the whole order!
				this.sendOrderToDeliver(fOrder); //after order is cooked sends it to delivery person.
			}
		}
		executor.shutdown();
		shutDownChef.countDown();
	}


	/**
	 * 
	 * @param futureOrder
	 */
	private void sendOrderToDeliver(Future<Order> futureOrder) {
		while(!futureOrder.isDone()) {
			//wait until it is done, meaning sleep
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			Order order = futureOrder.get();
			if (!(order.getOrderStatus()==COMPLETE)) {
				throw new IllegalStateException("illegal state for order");
			}
			_ordersToDeliver.put(order);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * 
	 * @return
	 */
	public double getEfficiency() {
		return _efficiencyRating;
	}

	/**
	 * activates and shuts down chef.
	 * @param activateChef
	 * @param shutDownChef
	 * @param pressureDecrease
	 * @param statistics
	 */
	public void initializeSemaphoreAndCountDownLatch(CountDownLatch shutDownChef,Semaphore pressureDecrease,Statistics statistics) {
		this.activateChef=new Semaphore(0);
		this.shutDownChef=shutDownChef;
		this.pressureDecrease=pressureDecrease;
		_statistics=statistics;

	}
	
	/**
	 * 
	 * @param n
	 */
	public void release(int n){
		activateChef.release(n);
	}

}
