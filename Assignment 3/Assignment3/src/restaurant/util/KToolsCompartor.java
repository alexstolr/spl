package restaurant.util;

import java.util.Comparator;

/**
 * used for sorting kitchen tools.
 * @author ALEX & Daniel
 *
 */
public class KToolsCompartor implements Comparator<KitchenTool> {

	@Override
	public int compare(KitchenTool o1, KitchenTool o2) {
		return o1.getName().compareTo(o2.getName());
	}

}
