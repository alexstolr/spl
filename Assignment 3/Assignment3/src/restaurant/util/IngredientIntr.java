package restaurant.util;
/**
 * 
 * @author Daniel & ALEX
 *
 */
public interface IngredientIntr {

	@Override
	public String toString();
	
	/**
	 * returns the name of the Ingredient
	 * @PRE none
	 * @post return _name
	 * @return the name of the Ingredient
	 */
	public String getName();
	
	/**
	 * return the quantity of the Ingredient
	 * @PRE none
	 * @post return _quantity
	 * @return the number of the Quantity
	 */
	public int getQuantity();
	
	/**
	 * sets quantity
	 * @PRE this._quantity
	 * @post post(this.quantity)== PRE (this._quantity)-quantity
	 * @param quantity
	 */
	public void setQuantity(int quantity);
	
	/**
	 * adds quantity to quantity
	 * adds given quantity to current quantity
	 * @param quantity
	 */
	public void addQuantity(int quantity);
}
