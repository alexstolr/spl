package restaurant.util;

/**
 * defines an ingredient Object
 * @author Daniel & ALEX
 *
 */
public class Ingredient implements IngredientIntr {
	private String _name;
	private int _quantity;

	/**
	 * constructor
	 * @param _name name of current ingredient
	 * @param _quantity quantity of current ingredient
	 */
	public Ingredient(String _name, int _quantity) {
		this._name = _name;
		this._quantity = _quantity;
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public int getQuantity() {
		return _quantity;
	}

	@Override
	public void setQuantity(int quantity) {
		_quantity = _quantity - quantity;
	}
	
	@Override
	public void addQuantity(int quantity){
		_quantity=_quantity+quantity;
	}
	
	@Override
	public String toString() {
		return "_name=" + _name + ", _quantity=" + _quantity;
	}

}
