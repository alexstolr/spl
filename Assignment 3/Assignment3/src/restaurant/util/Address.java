package restaurant.util;
import java.lang.Math;

/**
 * defines object Address
 * @author ALEX & Daniel
 */
public class Address {
	private double x;
	private double y;

	/**
	 *copy constructor 
	 */
	public Address(Address a) {
		x = a.getX();
		y = a.getY();
	}

	/**
	 * constructor
	 * @param x
	 * @param y
	 */
	public Address(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * default constructor
	 */
	public Address() {
		x=0;
		y=0;
	}

	/**
	 * calculates eucalidean distance between two points. 
	 * @param address
	 * @return Euclidean distance between two addresses.
	 */
	public int calculateDistance(Address address){
		return (int) Math.round(Math.sqrt(Math.pow((x-address.getX()), 2) + Math.pow(y-address.getY(),2)));
	}

	/**
	 * returns x parameter of address
	 * @return y
	 */
	public double getX() {
		return x;
	}

	/**
	 * returns y parameter of address
	 * @return y
	 */
	public double getY() {
		return y;
	}

	@Override
	public String toString() {
		return "Address [x = " + x + ", y = " + y + "]";
	}
}
