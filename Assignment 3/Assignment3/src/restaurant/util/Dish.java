package restaurant.util;
import java.util.Collections;
import java.util.Vector;
import java.util.ArrayList;

/**
 * describes a Dish in the restaurant.
 * @author Daniel & ALEX
 */
public class Dish {

	private final static int IN_PROGRESS = 2;
	private final static int COMPLETE = 3;

	private String _name;
	private int _cookTime;
	private ArrayList<Ingredient> _dishIngredients;
	private Vector<KitchenTool> _dishKTools;
	private int _difficultyRating;
	private double _reward;

	/**
	 * constructor of Dish.
	 * @param name
	 * @param cookTime
	 * @param difficultyRating
	 * @param reward
	 * @param dishIngredients
	 * @param dishKTools
	 */
	public Dish(String name,int cookTime, int difficultyRating, double reward,ArrayList<Ingredient> dishIngredients, Vector<KitchenTool> dishKTools){
		_difficultyRating=difficultyRating;
		_cookTime=cookTime;
		_dishIngredients =new ArrayList<Ingredient>();
		_dishIngredients.addAll(dishIngredients); // copy constructor
		_dishKTools=new Vector<KitchenTool>();
		_dishKTools.addAll(dishKTools);
		_reward=reward;
		_name=name;
	}

	/**
	 * return name of dish
	 * @return _name
	 */
	public String get_name() {
		return _name;
	}

	/**
	 * returns cooking time of dish
	 * @return _cookTime
	 */
	public int get_cookTime() {
		return _cookTime;
	}

	/**
	 * returns difficulty rating of dish
	 * @return _difficultyRating
	 */
	public int get_difficultyRating() {
		return _difficultyRating;
	}
	/**
	 * returns an ingredient at index i from arraylist dishingredients of current dish.
	 * @param i
	 * @return _dishIngredients
	 */
	public Ingredient getDishIngredient(int i){
		return _dishIngredients.get(i);
	}

	/**
	 * returns kitchen tool found in i'th index in _dishKTools vector
	 * @param i
	 * @return KitchenTool
	 */
	public KitchenTool getDishkTool(int i){
		return _dishKTools.get(i);
	}

	/**
	 * returns reward
	 * @return _reward
	 */
	public double get_reward() {
		return _reward;
	}

	/**
	 * the size of _dishIngredients Array.
	 * @return the size of _dishIngredients Array.
	 */
	public int ingredientSize(){
		return _dishIngredients.size();
	}

	/**
	 * the size of_dishKTools Vector.
	 * @return the size of_dishKTools Vector.
	 */
	public int kToolSize(){
		return _dishKTools.size();
	}

	@Override
	public boolean equals(Object dish){
		return	this.toString().equals(dish.toString());
	}
	
	
	/**
	 *  sets the Dish Status.
	 * @param status - IN_PROGRESS , COMPLETE
	 * @throws Exception
	 */
	public void setDishStatus(int status) throws Exception{
		if(status==IN_PROGRESS) status=IN_PROGRESS;
		else if(status==COMPLETE) status=COMPLETE;
		else {
			Exception e=new Exception("no such status");
			throw e;
		}
	}
	
	@Override
	public String toString() {
		return "Dish [_name=" + _name + ", _cookTime=" + _cookTime
				+ ", _dishIngredients=" + _dishIngredients + ", _dishKTools="
				+ _dishKTools + ", _difficultyRating=" + _difficultyRating
				+ ", _reward=" + _reward + "]";
	}

	/**
	 * sorts the kTool vector
	 */
	public void sort(){ 
		ArrayList<KitchenTool> kTools=new ArrayList<KitchenTool>();
		kTools.addAll(_dishKTools);
		_dishKTools.clear();
		KToolsCompartor compartor=new KToolsCompartor();
		Collections.sort(kTools,compartor);
		_dishKTools.addAll(kTools);
	}

}
