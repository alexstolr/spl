package restaurant.util;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * this class manages the running of the program.
 * @author ALEX & Daniel
 *
 */
public class Management {

	private final static int INCOMPLETE = 1;
	private final static int IN_PROGRESS = 2;
	private final static int DELIVERED = 4;

	private Statistics statistics;
	private Vector<Order> orders;
	private CountDownLatch shutDownChef;
	private Menu menu;
	private Warehouse warehouse;
	private Vector<RunnableChef> runnableChefs;
	private Vector<RunnableDeliveryPerson> runnableDelPer;
	private LinkedBlockingQueue<Order> _ordersToDeliver;
	private Semaphore pressureDecrease;
	private CountDownLatch delPerDone;

	/**
	 * constructor
	 */
	public Management() {
		this.orders = new Vector<Order>();
		this.runnableChefs = new Vector<RunnableChef>();
		this.runnableDelPer = new Vector<RunnableDeliveryPerson>();
	}

	/**
	 * initializer of some fields.
	 * @param statistics
	 * @param menu
	 * @param warehouse
	 */
	public void initialize(Statistics statistics, Menu menu, Warehouse warehouse){
		this.statistics = statistics;
		this.menu = menu;
		this.warehouse = warehouse;
		pressureDecrease = new Semaphore(0);
	}

	/**
	 * Takes new orders from orders to cook and sends them to the appropriate chef in order
	 * to cook them
	 */
	public void sendOrdersToCook(){
		Driver.LOGGER.info("System contains: [chefs="+ runnableChefs.size()+ "][deliveryPeople="+runnableDelPer.size()+"][orders="+orders.size()+"]");
		int c = orders.size(); //used to count down how many orders left that weren't started
		this.runChefs(); //initiates all threads for all chefs.
		this.runDelPer();
		Driver.LOGGER.info("Orders To Cook:" + c);
		while(c > 0){	//not all orders were started.
			for (int i = 0; i < orders.size(); i++) { // runs on all orders.
				if(getOrder(i).getOrderStatus() == INCOMPLETE){	//if i'th order is incomplete(not started yet).
					Driver.LOGGER.info("Attempting to send order: [" + getOrder(i).getOrderId() + "][numberOfMeals=" + getOrder(i).numOfMeals() + "]");
					for(int j = 0 ; j < runnableChefs.size() && getOrder(i).getOrderStatus() == INCOMPLETE ; j++){ //check for each chef & if order was not taken yet.
						runnableChefs.get(j).takeOrder(getOrder(i)); //if chef can take the order, take it.
						if(this.getOrder(i).getOrderStatus() == IN_PROGRESS){ 
							c--;
							Driver.LOGGER.info("Orders To Cook:" + c);
						}						
					}// here was - if order still not in progress after iteration on all chefs - go to sleep?

					if(getOrder(i).getOrderStatus() == INCOMPLETE && c == 1){ // if there are more orders then 1, don't sleep, iterate over all of them again
						try {
							Driver.LOGGER.info("Nothing to do.. going to sleep.");
							this.pressureDecrease.acquire();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		boolean done = false;
		while(!done){
			if(chefAndDelPerFinished(orders) == true){
				try {
					done = true;
					this.shouldStopChefsAndDeliveryPersons();
					shutDownChef.await();
				} catch (InterruptedException e) {
					System.out.println("ERROR!!!!:  "+e);
					e.printStackTrace();
				}

			}
			else{
				try {
					delPerDone.await();
				} catch (InterruptedException e) {
					System.out.println(e + "ERROR, some order not finished yet");
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * checks if all chef and delivery persons are done
	 * @param orders
	 * @return
	 */
	private boolean chefAndDelPerFinished(Vector<Order> orders){
		boolean allDelivered = true;
		for (int i = 0; i < orders.size(); i++) {
			if(orders.get(i).getOrderStatus() != DELIVERED)
				allDelivered = false;
		}
		return allDelivered;
	}

	/**
	 * stop all chef and delivery persons thread.
	 */
	public void shouldStopChefsAndDeliveryPersons(){
		for(int i=0; i <runnableChefs.size();i++){
			runnableChefs.get(i).Stop();
			runnableChefs.get(i).release(2);
		}
		for(int i=0;i<runnableDelPer.size();i++){
			runnableDelPer.get(i).terminate();
			try{
				_ordersToDeliver.put(new Order("-1"));
			}
			catch(InterruptedException e){	

			}
		}
	}

	/**
	 * initiates all Delivery Persons in different threads.
	 */
	private void runDelPer(){
		for (int i = 0; i < runnableDelPer.size(); i++) {
			Thread t=new Thread(runnableDelPer.get(i));
			t.start();
		}
	}

	/**
	 * initiates all chefs in different threads.
	 */
	private void runChefs() {
		for (int i = 0; i < runnableChefs.size(); i++) {
			Thread t=new Thread(runnableChefs.get(i));
			t.start();
		}
	}

	/**
	 * return statistics.
	 * @return statistics.
	 */
	public Statistics getStatistics() {
		return statistics;
	}

	/**
	 * returns an order in the i'th place from orders vector.
	 * @param i index.
	 * @return Order
	 */
	public Order getOrder(int i){
		return orders.get(i);
	}

	/**
	 * 
	 * @param i
	 * @return
	 */
	public RunnableChef getChef(int i){
		return runnableChefs.get(i);
	}

	/**
	 * return menu
	 * @return menu
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * returns warehouse
	 * @return warehouse
	 */
	public Warehouse getWarehouse() {
		return warehouse;
	}
	
	/**
	 * adds runnable chef to vector of runnablechefs.
	 * @param chef
	 */
	public void addRunnableChefs(RunnableChef chef) {
		runnableChefs.add(chef);
	}

	/**
	 * adds orders to orders vector
	 * @param order
	 */
	public void addOrder(Order order) {
		orders.add(order);
	}

	/**
	 * adds delivery persons to runnableDelPer vector
	 * @param delPer
	 */
	public void addRunnaleDelPer(RunnableDeliveryPerson delPer) {
		runnableDelPer.add(delPer);
	}

	/**
	 * returns the size of runnableChefs vector
	 * @return
	 */
	public int getChefsCount(){
		return runnableChefs.size();
	}

	/**
	 * initializes semaphore activeateChef, CountDownLatch shutDownChef and LinkedBlockingQueue<Order> ordersToDeliver with correct data.
	 * @param activateChef
	 * @param shutDownChef
	 * @param ordersToDeliver
	 */
	public void PostParserInitialize(Semaphore activateChef,CountDownLatch shutDownChef,LinkedBlockingQueue<Order> ordersToDeliver){
		this._ordersToDeliver=ordersToDeliver;
		this.shutDownChef=shutDownChef;
		delPerDone=new CountDownLatch(orders.size());
		for(int i=0;i<runnableChefs.size();i++){
			runnableChefs.get(i).initializeSemaphoreAndCountDownLatch(shutDownChef,this.pressureDecrease,this.statistics);
		}
		for(int i=0;i<runnableDelPer.size();i++){
			runnableDelPer.get(i).initializeCountDownLatch(this.delPerDone);
		}
		this.SortAllKtoolsOrders();
	}
	
	/**
	 * sorts all kitchen tools used in this order alphabetically here
	 */
	public void SortAllKtoolsOrders(){
		for (int i = 0; i < orders.size(); i++) {
			this.orders.elementAt(i).ktoolsSortOfAllThe_Dishes();
		}

	}

}
