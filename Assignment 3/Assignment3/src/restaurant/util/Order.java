package restaurant.util;
import java.util.Vector;

/**
 * represents a a menu in the restaurant.
 * @author Daniel & ALEX
 */
public class Order {

	private final static int INCOMPLETE = 1;
	private final static int IN_PROGRESS = 2;
	private final static int COMPLETE = 3;
	private static final int DELIVERED = 4;

	private String orderId;
	private int difficultyRating;
	private int  orderStatus;
	private Vector<OrderOfDish> ordersOfDish;
	private Address customerAddress;
	private long realCookTime;
	private long expectedCookTime;
	private double reward;

	/**
	 * constructor
	 * @param orderId 
	 * @param difficultyRating
	 * @param ordersOfDish
	 * @param customerAddress
	 */
	public Order(String orderId, int difficultyRating, Vector<OrderOfDish> ordersOfDish, Address customerAddress) {
		this.orderId = orderId;
		this.difficultyRating =difficultyRating;
		this.orderStatus = INCOMPLETE;
		this.ordersOfDish = new Vector<OrderOfDish>();
		this.ordersOfDish.addAll(ordersOfDish);
		this.customerAddress = customerAddress;
		this.seteDifficultyRating();
		this.updateReward();
		this.updateExpectedCookTime();
	}
	public Order(String id){
		this.orderId=id;
		this.difficultyRating =0;
		this.orderStatus = INCOMPLETE;
		this.ordersOfDish = new Vector<OrderOfDish>();
		this.customerAddress = new Address();
		this.seteDifficultyRating();
	}
	/**
	 * returns the order ID
	 * @return the order id
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * returns the orders difficulty rating
	 * @return difficulty rating
	 */
	public int getDifficultyRating() {
		return difficultyRating;
	}

	/**
	 * 
	 * @return the order status
	 */
	public int getOrderStatus() {
		return orderStatus;
	}
	/**
	 * returns the time that took to cook the order.
	 * @return Real Time Cook
	 */
	public long getRealTimeCook() {
		return realCookTime;
	}
	/**
	 * set reat cook time
	 * sets the real time cook
	 * @param realTimeCook
	 */
	public void setRealTimeCook(long realTimeCook) {
		this.realCookTime = realTimeCook;
	}

	/**
	 * sets the order status.
	 * @param magicNumber represents the new status of the order.
	 * @throws Exception
	 */
	public void setOrderStatus(int magicNumber) throws Exception {
		if(magicNumber==IN_PROGRESS) orderStatus=IN_PROGRESS;
		else if(magicNumber==COMPLETE) orderStatus=COMPLETE;
		else if(magicNumber == DELIVERED) orderStatus = DELIVERED; 
		else {
			Exception e=new Exception("no such status");
			throw e;
		}
	}

	/**
	 * returns order of dish in i'th index
	 * @param i
	 * @return the i'th order.
	 */
	public OrderOfDish getOrderOfDish(int i) {
		return this.ordersOfDish.get(i);
	}
	/**
	 * returns the address of the customer.
	 * @return customer Address
	 */
	public Address getCustomerAddress() {
		return customerAddress;
	}

	/**
	 * sets the difficulty rating
	 * sets difficulty rating
	 */
	public void seteDifficultyRating(){
		int difficulty = 0;
		for (int i = 0; i < ordersOfDish.size(); i++) {
			difficulty = difficulty+ordersOfDish.get(i).get_dish().get_difficultyRating();
		}
		this.difficultyRating= difficulty;
	}

	/**
	 * returns reward.
	 * @return the reward order
	 */
	public double getReward() {
		return reward;
	}

	/**
	 * sets reward.
	 * @param reward
	 */
	public void setReward() {
		for (int i = 0; i < ordersOfDish.size(); i++) {
			reward=reward+(ordersOfDish.get(i).getQuantity()*ordersOfDish.get(i).get_dish().get_reward());
		}
	}
	/**
	 * returns the expected cooking time 
	 * @return the Expected Cook Time
	 */
	public long getExpectedCookTime() {
		return expectedCookTime;
	}

	/**
	 * returns the size of ordersOfDish vector
	 * @return
	 */
	public int OrderOfDishSize() {
		return ordersOfDish.size();
	}
	/**
	 * updating the reward of the order.
	 */
	private void updateReward() {
		for (int i = 0; i < ordersOfDish.size(); i++) {
			reward=reward+(double)(ordersOfDish.get(i).getQuantity()*ordersOfDish.get(i).get_dish().get_reward());
		}
	}
	/**
	 * returns the number of the meals in the order.
	 * @return the number of the meals in the order.
	 */
	public int numOfMeals(){
		int num=0;
		for (int i = 0; i < ordersOfDish.size(); i++) {
			num=num+ordersOfDish.get(i).getQuantity();
		}
		return num;
	}
	
	/**
	 * sorts all the kitchen tools in the order in alphabetic order.
	 */
	public void ktoolsSortOfAllThe_Dishes(){
		for (int i = 0; i < ordersOfDish.size(); i++) {
			ordersOfDish.get(i).get_dish().sort();
		}
	}
	
	/**
	 * updates expected cooking time
	 */
	private void updateExpectedCookTime() {
		long maxExpectedCookTime = 0;
		for (int i = 0; i < OrderOfDishSize(); i++) {
			if(maxExpectedCookTime < ordersOfDish.get(i).get_dish().get_cookTime()){
				maxExpectedCookTime =(long)(ordersOfDish.get(i).get_dish().get_cookTime());
			}
		}
		expectedCookTime=maxExpectedCookTime;
	}
	@Override
	public String toString() {
		return "\t" + "\t" + "[Order] orderId: " + orderId + "; difficultyRating: "
				+ difficultyRating + "; orderStatus=" + orderStatus
				+ ", ordersOfDish=" + ordersOfDish + ", customerAddress="
				+ customerAddress + ", realCookTime=" + realCookTime
				+ ", expectedCookTime=" + expectedCookTime + ", reward="
				+ reward + "]";
	}
	

	
}
