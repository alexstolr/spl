package restaurant.util;
import java.util.ArrayList;
import java.util.Vector;

public class Warehouse implements WarehouseIntr {

	private Vector<KitchenTool> _kitchenTool;
	private ArrayList<Ingredient> _ingredient;

	public Warehouse() {
		_kitchenTool = new Vector<KitchenTool>();
		_ingredient = new ArrayList<Ingredient>();
	}

	@Override
	public Ingredient getIngredient(String ingredient) {
		for (int i = 0; i < _ingredient.size() ; i++) {
			if(_ingredient.get(i).getName().equals(ingredient)){
				return _ingredient.get(i);
			}
		}
		return null;
	}

	/**
	 * a method that is used only in the test (for teardown)
	 */
	@Override
	public void clearWareHouse() {
		_ingredient.clear();
		_kitchenTool.clear();
		
	}

	@Override
	public KitchenTool getKTool(String kTool) {
		for (int i = 0; i < _kitchenTool.size() ; i++) {
			if(_kitchenTool.get(i).getName().equals(kTool)){
				return _kitchenTool.get(i);
			}
		}
		return null;
	}


	@Override
	/**
	 * returns the quantity of a given tool
	 */
	public int getKitchenToolQuantity(String name) {
		for (int i = 0; i < _kitchenTool.size(); i++) {
			if(name.equals((_kitchenTool.get(i)).getName())){
				return _kitchenTool.get(i).getQuantity();
			}
		}
		return -1;
	}
	
	@Override
	public void decreaseIngredientQuantity(int quantity,String name) {
		boolean flag=false;
		for (int i = 0; i < _ingredient.size() && !flag; i++) {
			if(name.equals((_ingredient.get(i)).getName())){
				(_ingredient.get(i)).setQuantity(quantity);
				flag=true;
			}
		}
		if(!flag){
			Exception e = new Exception(name + " not found"); 
			System.out.println(e);
		}
	}

	@Override
	public int getKToolIndex(String Ktool) {
		for (int i = 0; i < _kitchenTool.size(); i++) {
			if(_kitchenTool.get(i).getName().equals(Ktool)){
				return i;
			}
		}
		return -1; 
	}

	@Override
	public void addIngredient(Ingredient ingredient) {
		_ingredient.add(ingredient);

	}

	@Override
	public void addKitchenTool(KitchenTool kitchenTool) {
		_kitchenTool.add(kitchenTool);

	}

}
