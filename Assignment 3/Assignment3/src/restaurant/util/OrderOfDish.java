package restaurant.util;

/**
 * describes the current order of a dish - which dish, and what quantity of it.
 * @author ALEX & Daniel
 *
 */
public class OrderOfDish {

	private Dish _dish;
	private int _quantity;
	
	/**
	 *constructor 
	 * @param dish
	 * @param quantity
	 */
	public OrderOfDish(Dish dish,int quantity){
		_dish = dish;
		_quantity = quantity;		
	}

	/**
	 * getter for quantity of the dish in current order.
	 * @return
	 */
	public int getQuantity(){
		return _quantity;
	}

	/**
	 * getter for Dish in current order.
	 * @return
	 */
	public Dish get_dish() {
		return _dish;
	}

	@Override
	public String toString() {
		return "\t" + "\t" + "[OrderOfDish]" + "\n" + "[Dish]" + _dish.toString() + "quantity=" + _quantity + "]" + "\n" + "[OrderOfDish]";
	}



}
