package restaurant.util;
import java.util.concurrent.Semaphore;

/**
 * this class represents a kitchen tool object.
 * @author ALEX & Daniel
 *
 */
public class KitchenTool implements KitchenToolIntr {
	private Semaphore _s;
	private String _name;
	private int _quantity;
	
	/**
	 * constructor
	 * @param _name
	 * @param _quantity
	 */
	public KitchenTool(String _name, int _quantity) {
		this._name = _name;
		this._quantity = _quantity;
		_s = new Semaphore(_quantity);
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public int getQuantity() {
		return _quantity;
	}


	@Override
	public void setQuantity(int quantity) {
		_quantity = _quantity + quantity; 
	}

	@Override
	public String toString() {
		return "name = " + _name + " , quantity = " + _quantity;
	}

	/**
	 * Releases the given number of permits, returning them to the semaphore.
	 * Override to prevent access to semaphore from outside
	 * @param n
	 */
	public void acquire(int n){
		try{
			_s.acquire(n);
		}
		catch (InterruptedException e){}

	}

	/**
	 * Releases the given number of permits, returning them to the semaphore. 
	 * @param n
	 */
	public void release(int n){
			_s.release(n);
	}

}