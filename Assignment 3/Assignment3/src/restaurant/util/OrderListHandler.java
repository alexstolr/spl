package restaurant.util;

import java.util.List;
import java.util.Vector;

import org.jdom2.Document;
import org.jdom2.Element;

/**
 * this class handle parsing of orderList.xml
 * @author ALEX & Daniel
 *
 */
public class OrderListHandler {

	Document ordersListDoc;
	/**
	 * Constructor
	 * @param ordersListDoc
	 */
	public OrderListHandler(Document ordersListDoc) {
		this.ordersListDoc = ordersListDoc;
	}

	/**
	 * parses orderList and Orders elements
	 * @param management
	 * @param initialDataDoc
	 */
	public static void parseOrderListXml(Management management, Document initialDataDoc){
		Element orderList = initialDataDoc.getRootElement();
		Element orders = orderList.getChild("Orders");
		handleOrderParsing(orders, management);
	}

	/**
	 * Parses Order element
	 * @param orders
	 * @param management
	 */
	public static void handleOrderParsing(Element orders, Management management){
		List<Element> order = orders.getChildren();
		for (int i = 0; i < order.size(); i++) {
			Vector<OrderOfDish> ordersOfDish = new Vector<OrderOfDish>();
			Element currOrder = (Element)order.get(i);
			String orderId = currOrder.getAttributeValue("id");
			handleDishesParsing(currOrder, management, ordersOfDish);
			management.addOrder(new Order(orderId, 0, ordersOfDish, handleDeliveryAddressParsing(currOrder, management)));
		}
	}

	/**
	 * parses DeliveryAddress element
	 * @param order
	 * @param management
	 */
	public static Address handleDeliveryAddressParsing(Element order, Management management){
		Element deliveryAddress = order.getChild("DeliveryAddress");
		Element x = deliveryAddress.getChild("x");
		Element y = deliveryAddress.getChild("y");
		int xParam = Integer.parseInt(x.getText());
		int yParam = Integer.parseInt(y.getText());
		return new Address(xParam, yParam);
	}

	/**
	 * parses dish element
	 * @param orders
	 * @param management
	 */
	public static void handleDishesParsing(Element orders, Management management, Vector<OrderOfDish> ordersOfDish){
		Element dish = orders.getChild("Dishes");
		List<Element> Dishes = dish.getChildren();
		for (int i = 0; i < Dishes.size(); i++) {
			Element currDish = Dishes.get(i);
			Element name = currDish.getChild("name");
			Element quantity = currDish.getChild("quantity");
			Dish newDish = management.getMenu().getDish(name.getText());
			ordersOfDish.add(new OrderOfDish(newDish, Integer.parseInt(quantity.getText())));
		}
	}

	

}
