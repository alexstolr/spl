package restaurant.util;

/**
 * 
 * @author Daniel & ALEX
 *
 */
public interface KitchenToolIntr {
	@Override
	public String toString();

	/**
	 * return the name of the kitchen tool
	 * @PRE none
	 * @post return _name
	 * @return the name of the kitchen tool
	 */
	public String getName();

	/**
	 * returns the quantity of the kitchen tool
	 * @PRE none
	 * @post return _quantity
	 * @return the quantity of the kitchen tool
	 */
	public int getQuantity();

	/**
	 * sets quantity
	 */
	public void setQuantity(int quantity);

}
