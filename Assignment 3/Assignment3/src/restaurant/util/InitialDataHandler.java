package restaurant.util;

import java.util.List;


import java.util.concurrent.LinkedBlockingQueue;

import org.jdom2.Document;
import org.jdom2.Element;

/**
 * this class handles parsing of InitialData.xml
 * @author Alex & Daniel
 *
 */
public class InitialDataHandler {
	Document initialDataDoc;

	/**
	 * constructor
	 * @param initialDataDoc
	 */
	public InitialDataHandler(Document initialDataDoc) {
		this.initialDataDoc = initialDataDoc;
	}

	/**
	 * parses restaurant element
	 * @param management
	 * @param initialDataDoc
	 */
	public static void parseInitialDataXml(Management management, Document initialDataDoc, LinkedBlockingQueue<Order> ordersToDeliver){
		Element restaurant = initialDataDoc.getRootElement(); // this is the root element in my case 'restaurant'
		Warehouse warehouse = new Warehouse();
		Address address = new Address(handleAddressParsing(restaurant));
		handleRepositoryParsing(restaurant, warehouse);
		Statistics statistics = new Statistics();
		management.initialize(statistics,new Menu(),warehouse);
		handleStaffParsing(restaurant,management,address, ordersToDeliver,warehouse,statistics);
	}

	/**
	 * handles parsing of address element
	 * @param element
	 * @return Address
	 */
	private static Address handleAddressParsing(Element element){
		Element addressElement = element.getChild("Address");
		int x = Integer.parseInt(addressElement.getChild("x").getText());
		int y = Integer.parseInt(addressElement.getChild("y").getText());
		return new Address(x, y);
	}

	/**
	 * handles repository element
	 * @param element
	 * @param warehouse
	 */
	private static void handleRepositoryParsing(Element element,Warehouse warehouse){
		Element repository = element.getChild("Repository");
		handleToolOrIngredient(repository, warehouse, "Tools");
		handleToolOrIngredient(repository, warehouse, "Ingredients");
	}

	/**
	 * handles kitchentool or ingredient element
	 * @param element
	 * @param warehouse
	 * @param elementName
	 */
	private static void handleToolOrIngredient(Element element, Warehouse warehouse, String elementName){
		Element toolsOrIngs = element.getChild(elementName); // toolsOrIngs - is Tools OR Ingredients Element
		List<Element> elementsList = toolsOrIngs.getChildren(); // list of children of toolsOrIngs.
		for (int i = 0; i < elementsList.size(); i++) {
			Element toolsOrIngsChild = elementsList.get(i);
			Element name = toolsOrIngsChild.getChild("name");
			Element quantity = toolsOrIngsChild.getChild("quantity");
			if(elementsList.get(i).getName().equals("KitchenTool")){
				warehouse.addKitchenTool(new KitchenTool(name.getText(), Integer.parseInt(quantity.getText())));	
			}
			else{
				warehouse.addIngredient(new Ingredient(name.getText(), Integer.parseInt(quantity.getText())));	
			}
		}
	}

	/**
	 * handles staff elements parsing
	 * @param element
	 * @param management
	 * @param address
	 * @param ordersToDeliver
	 */
	private static void handleStaffParsing(Element element, Management management, Address address, LinkedBlockingQueue<Order> ordersToDeliver,Warehouse warehouse,Statistics statistics){
		Element staff = element.getChild("Staff");
		handleChefsParsing(staff, management, ordersToDeliver,warehouse);
		handleDeliveryPersonalsParsing(staff, management, address, ordersToDeliver,statistics);
	}

	/**
	 * handles parsing of chef element
	 * @param element
	 * @param management
	 * @param ordersToDeliver
	 */
	private static void handleChefsParsing(Element element, Management management, LinkedBlockingQueue<Order> ordersToDeliver, Warehouse warehouse){
		Element chefs = element.getChild("Chefs");
		List<Element> chefElements = chefs.getChildren(); // list of children of chefs.
		for (int i = 0; i < chefElements.size(); i++) {
			Element currentChef = chefElements.get(i); // is current cheff
			Element name = currentChef.getChild("name");
			Element efficiencyRating = currentChef.getChild("efficiencyRating");
			Element enduranceRating = currentChef.getChild("enduranceRating");
			RunnableChef chef = new RunnableChef(name.getText(), Double.parseDouble(efficiencyRating.getText()),Integer.parseInt(enduranceRating.getText()),0, management.getStatistics(), warehouse,ordersToDeliver);
			management.addRunnableChefs(chef);
		}
	}

	/**
	 * handles parsing of delivery person element
	 * @param element
	 * @param management
	 * @param address
	 * @param ordersToDeliver
	 */
	private static void handleDeliveryPersonalsParsing(Element element, Management management, Address address, LinkedBlockingQueue<Order> ordersToDeliver,Statistics statistics){
		Element deliveryPersonals = element.getChild("DeliveryPersonals");
		List<Element> DeliveryPerson = deliveryPersonals.getChildren(); // list of children of delivery persons.
		for (int i = 0; i < DeliveryPerson.size(); i++) {
			Element currentDeliveryPersonal = DeliveryPerson.get(i); // is current delivery person
			Element name = currentDeliveryPersonal.getChild("name");
			Element speed = currentDeliveryPersonal.getChild("speed");
			RunnableDeliveryPerson delPer = new RunnableDeliveryPerson(name.getText(), address,Integer.parseInt(speed.getText()),ordersToDeliver, statistics);
			management.addRunnaleDelPer(delPer);
		}
	}

}
