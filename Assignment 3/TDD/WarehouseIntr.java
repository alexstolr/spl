package restaurant.util;

/**
 * @author Daniel & Alex
 * Warehouse is an object that contains all the kitchen tool and ingredients.
 */
public interface WarehouseIntr {
	
	/**
	 * @pre none
	 * @param ingredient
	 * @return ingredient
	 */
	public Ingredient getIngredient(String ingredient);

	
	/**
	 * @pre none
	 * @param KTool
	 * @return KTool
	 */
	public KitchenTool getKTool(String KTool);

	/**
	 * @pre: this.getIngArr.size
	 * @post: post(this.getIngArr.size) == pre(this.getIngArr.size) + 1
	 * @param ingredient
	 */
	public void addIngredient(Ingredient ingredient);

	/**
	 * @pre:this.getKToolVec.size
	 * @post: post(this.getKToolVec.size()) == pre(this.getKToolVec.size()) + 1
	 * @post:this.getKToolVec().getElemnt(this.getKToolVec().size()-1).toString().equals(kitchenTool.toString())
	 * @post:@pre(this.getKToolVec.getElemnt(this.getKToolVec.size()-1).toString()).equals(@post(this.getKToolVec.getElemnt(this.getKToolVec.size()-2).toString()))
	 * @param kitchenTool 
	 */
	public void addKitchenTool(KitchenTool kitchenTool);
	
	/**
	 * @pre: this.getIngArr.getElemnt(name_position).(name_position-it's the index of the name Ingredient
	 * in this.getIngArr()).getQuantity();
	 * @post @post(this.getIngArr.getElemnt(name_position).(name_position-it's the index of the name Ingredient
	 * in this.getIngArr()).getQuantity())==@pre(this.getIngArr.getElemnt(name_position).(name_position-it's the index of the name Ingredient
	 * in this.getIngArr()).getQuantity())+quantity
	 * @param quantity
	 * @param name
	 */
	public int getKitchenToolQuantity(String name); 
	
	/**
	 * @pre: this.getKToolVec().getElemnt(name_position).(name_position-it's the index of the name Ingredient
	 * in this.getKToolVec()).getQuantity();
	 * @post @post(this.getKToolVec().getElemnt(name_position).(name_position-it's the index of the name Ingredient
	 * in this.getKToolVec()).getQuantity())==pre(this.getKToolVec().getElemnt(name_position).(name_position-it's the index of the name Ingredient
	 * in this.getIngArr()).getQuantity())-quantity
	 * @param quantity
	 * @param name
	 */
	public void deacreaseIngredientQuantity(int quantity,String name);
	
	public int getKToolIndex(String Ktool);
	
	public void clearWareHouse();







}
