package restaurant.util;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class WarehouseTest {
	 
	 Warehouse warehouse;

	@Before
	public void setUp() throws Exception {
		this.warehouse = new Warehouse();
	}

	@After
	public void tearDown() throws Exception {
		warehouse.clearWareHouse();
	}

	//TODO check if needed
	/*@Test
	public void testWarehouse() {
		assertEquals(true,warehouse.getIngArr().isEmpty());
		assertEquals(true, warehouse.getKToolVec().isEmpty());
	}
*/
	@Test
	public void testGetIngredient() {
		warehouse.addIngredient(new Ingredient("fanta", 5));
		assertEquals(true,warehouse.getIngredient("fanta").getName()=="fanta");
		warehouse.addIngredient(new Ingredient("coco beans A", 7));
		assertEquals(true,warehouse.getIngredient("coco beans A").getName()=="coco beans A");
		warehouse.addIngredient(new Ingredient("coco beans B", 8));
		assertEquals(true,warehouse.getIngredient("coco beans B").getName()=="coco beans B");
	}

	@Test
	public void testGetKTool() {
		warehouse.addKitchenTool(new KitchenTool("spoon", 3));
		warehouse.addKitchenTool(new KitchenTool("pot", 4));
		assertEquals(false,warehouse.getKTool("spoon").getName()=="fanta");
		assertEquals(true,warehouse.getKTool("spoon").getName()=="spoon");
		warehouse.addKitchenTool(new KitchenTool("Rolling Pin", 3));
		assertEquals(true,warehouse.getKTool("Rolling Pin").getName()=="Rolling Pin");
		warehouse.addKitchenTool(new KitchenTool("fork", 10));
		warehouse.addKitchenTool(new KitchenTool("plate", 5));
		assertEquals(true,warehouse.getKTool("fork").getName()=="fork");
		assertEquals(true,warehouse.getKTool("plate").getName()=="plate");
	}
	public void testAddIngredient() {
		warehouse.addIngredient(new Ingredient("fanta", 5));
		assertEquals(true,warehouse.getIngredient("fanta").getName()=="fanta");
		warehouse.addIngredient(new Ingredient("coco beans A", 7));
		assertEquals(true,warehouse.getIngredient("coco beans A").getName()=="coco beans A");
		warehouse.addIngredient(new Ingredient("coco beans B", 8));
		assertEquals(true,warehouse.getIngredient("coco beans B").getName()=="coco beans B");
	}
	public void testAddKitchenTool(){
		
		
	}
    public void testGetKGetitchenToolQuantity(){
    	warehouse.addKitchenTool(new KitchenTool("spoon", 4));
    	warehouse.addKitchenTool(new KitchenTool("pot", 1));
    	warehouse.addKitchenTool(new KitchenTool("Rolling Pin", 3));
    	warehouse.addKitchenTool(new KitchenTool("chop stick", 5));
    	warehouse.addKitchenTool(new KitchenTool("knife", 4));
    	warehouse.addKitchenTool(new KitchenTool("mixer", 10));
    	assertEquals(true,warehouse.getKitchenToolQuantity("pot")==1);
    	assertEquals(true,warehouse.getKitchenToolQuantity("spoon")==4);
    	assertEquals(true,warehouse.getKitchenToolQuantity("mixer")==9);
    	assertEquals(true,warehouse.getKitchenToolQuantity("knife")==1);
    	assertEquals(true,warehouse.getKitchenToolQuantity("chop stick")==5);
    	assertEquals(true,warehouse.getKitchenToolQuantity("Rolling Pin")==3);
	}
    public void testDeacreaseIngredientQuantity(){
    	warehouse.addIngredient(new Ingredient("fanta", 3));
    	assertEquals(true,warehouse.getIngredient("fanta").getQuantity()==3);
    	warehouse.addIngredient(new Ingredient("coca beans A", 6));
    	assertEquals(true,warehouse.getIngredient("coca beans A").getQuantity()==6);
    	warehouse.addIngredient(new Ingredient("coca beans B", 7));
    	assertEquals(true,warehouse.getIngredient("coca beans B").getQuantity()==7);
    	warehouse.deacreaseIngredientQuantity(2, "fanta");
    	warehouse.deacreaseIngredientQuantity(3, "coca beans A");
    	warehouse.deacreaseIngredientQuantity(2, "coca beans B");
    	assertEquals(true,warehouse.getIngredient("fanta").getQuantity()==1);
    	warehouse.deacreaseIngredientQuantity(3, "coca beans A");
    	assertEquals(false,warehouse.getIngredient("fanta").getQuantity()!=1);
    	assertEquals(true,warehouse.getIngredient("coca beans A").getQuantity()==3);
    	assertEquals(false,warehouse.getIngredient("coca beans A").getQuantity()!=3);
    	assertEquals(true,warehouse.getIngredient("coca beans B").getQuantity()==5);
	}
    
    public void testGetKToolIndex(){
    	warehouse.addKitchenTool(new KitchenTool("spoon", 3));
    	warehouse.addKitchenTool(new KitchenTool("pot", 4));
    	warehouse.addKitchenTool(new KitchenTool("Rolling Pin", 3));
    	warehouse.addKitchenTool(new KitchenTool("Pin", 3));
    	assertEquals(true,warehouse.getKToolIndex("spoon")==0);
    	assertEquals(true,warehouse.getKToolIndex("pot")==1);
    	assertEquals(false,warehouse.getKToolIndex("pot")==3);
    	assertEquals(false,warehouse.getKToolIndex("Rolling Pin")==2);
    	assertEquals(true,warehouse.getKToolIndex("Pin")==3);
    	assertEquals(true,warehouse.getKToolIndex("pot")==1);
    }
}
