package Server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import Encoder.*;
import Protocol.ServerProtocol;
import Protocol.StompProtocol;
import Stomp.StompFrame;
import Tokenizer.StompTokenizer;
import Tokenizer.StompTokenizerImpl;
 
public class ConnectionHandler implements Runnable {
 
	private BufferedReader in; //Input Stream.
	private PrintWriter out; //Output stream.
    private  Socket clientSocket;
    private  SimpleEncoder _encoder;
    private  StompTokenizerImpl tokenizer;
    private  StompProtocol protocol;
 
    public ConnectionHandler(Socket acceptedSocket, StompTokenizerImpl tokenizer, StompProtocol protocol,ServerData data) {
    	 this.clientSocket = acceptedSocket;
        _encoder = new SimpleEncoder("UTF-8");
        this.tokenizer = tokenizer;
        this.protocol= protocol;
        in=tokenizer.getBr();
    }
    
   /* public void init() throws UnsupportedEncodingException, IOException{
    	in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(),"UTF-8"));
        out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(),"UTF-8"), true);
    }*/
 
    public void run() {
    	try {
			tokenizer.init(clientSocket, _encoder);
		} catch (IOException e1) {
			System.out.println("Error " + e1);
			e1.printStackTrace();
		}
    	System.out.println("Client is connected to server. welcome to Alex and Daniel 's server.  VERSION 1.0");
		System.out.println("The client is from: " + clientSocket.getInetAddress() + ":" + clientSocket.getPort());
        while (!protocol.shouldClose() && !clientSocket.isClosed()) {                          
            try {
                if (!tokenizer.isAlive())
                    protocol.connectionTerminated();
                else {
                	//  Bytes b = read from socket
                	//  tokenizer.addBytes(b);
                	//if (tokenizer.hasMessage(){
                	//   StompFrame frame = tokenizer.nextMessage();
                	//   StompFrame respone = protocol.process(frame.execute());
                	//   if(response != null)
                	//		out.write(response.toString());
                	//}
                   // DataInputStream in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                	//InputStream stream = clientSocket.getInputStream();

                	// byte[] bytes = new byte[1024];
                	// in.read(bytes);
                	//String msg = tokenizer.nextToken();
                	StompFrame stompFrm = tokenizer.getFrame(in);
                	String ans = protocol.processMessage(tokenizer,"");//TODO change "" to msg
                    if (ans != null) {
                        byte[] buf = _encoder.toBytes(ans);
                        clientSocket.getOutputStream().write(buf, 0, buf.length);
                    }
                }
            } catch (IOException e) {
                protocol.connectionTerminated();
                break;
            }
        }
        try {
            clientSocket.close();
        } catch (IOException ignored) {
        }
        System.out.println("thread done");
    }
}