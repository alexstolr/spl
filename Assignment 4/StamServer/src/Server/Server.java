package Server;

import java.io.BufferedReader;
import java.io.IOException;


import java.io.InputStreamReader;
import java.net.ServerSocket;



import java.net.Socket;

import Protocol.StompProtocolFactory;
import Tokenizer.*;

public class Server implements Runnable {

	private ServerSocket serverSocket;
	private int listenPort;
	private ServerData data;

	/**
	 * 
	 * @param stompProtocolFactory
	 * @throws IOException
	 */
	public Server(int port, ServerData data)
			throws IOException {
		serverSocket = null;
		listenPort = port;
		this.data = data;
	}

	public void run() {
		try {
			serverSocket = new ServerSocket(listenPort);
			System.out.println("Listening (Waiting for client to connect)....");
		} catch (IOException e) {
			System.out.println("Cannot listen on port " + listenPort);
			e.printStackTrace(System.err);

		}
		while (true) {
			try {
				Socket clientSocket = serverSocket.accept();
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(),"UTF-8"));
				ServerConcurrencyModel concurrencyModel = new ThreadPerClient();
				ConnectionHandler newHandler = new ConnectionHandler(clientSocket, new StompTokenizerFactory().create(in), new StompProtocolFactory().create(), data);
				concurrencyModel.apply(newHandler);
			} catch (IOException e) {
				System.out.println("Failed to accept on port " + listenPort);
			}
		}
	}

	public static final void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.err.println("A single argument - a port, is required");
			System.exit(1);
		}
		int port = Integer.decode(args[0]).intValue();
		Server stompServer = new Server(port, new ServerData());
		Thread serverThread = new Thread(stompServer);
		serverThread.start();
		try {
			serverThread.join();
		} catch (InterruptedException e) {
			System.out.println("Server stopped");
		}
	}

}
