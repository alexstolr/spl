package Server;



import java.util.ArrayList;
import java.util.HashMap;

import Stomp.MessageFrame;

//TODO each user has a nickname and passcode. a list of topics? a list of followers
// each users has a list of topics he created??????
/**
 * 
 * @author ALEX & Daniel
 *
 */
public class User {
	
	Boolean isConnected; //used to know if user is connected. if false, messages will be sent to offlineMessages
	ArrayList<MessageFrame> offlineMessages;//messages the user needs to receive after connection.
	ArrayList<User> followersList; // list of user current user is following.
	HashMap<String,String> credentials;	//username and passcode
	
	
	/**
	 * constructor
	 */
	public User(){
		//TODO implement
	}

	/**
	 * register current User to Topic
	 */
	public void regToTopic(String topicName){
		//TODO implement, also, should this method be in server?
	}
	
	/**
	 * tells the server that current clients wants to follow User 'user'
	 */
	public void followUsr(String username){
		//TODO implement
	}
	
	/**
	 * tells the server that current clients wants to unfollow User 'user'
	 */
	public void unFollowUsr(String username){
		//TODO implement
	}
	
	/**
	 * returns user's name.
	 * @return userName
	 */
	public String getUserName(){
		return null;
	}
	
	public void tweet(){
		//TODO implement
	}
}
