package Server;

interface ServerConcurrencyModel {
   public void apply (Runnable connectionHandler); 
}