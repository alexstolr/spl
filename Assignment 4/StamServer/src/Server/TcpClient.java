package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import Encoder.Encoder;
import Encoder.SimpleEncoder;


public class TcpClient {

	private static void run(String serverName, int port) {
		try {
			//String connect = "CONNECT" + "\n" + "accept-version:1.2" + "\n"  + "host:127.0.0.1 + " + "\n"  + "login:username" + "\n" + "passcode:password";
			Encoder encoder = new SimpleEncoder("UTF-8");
			InetAddress address = InetAddress.getByName(serverName);
			Socket socket = new Socket(address, port);
			String line;
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			// translate each character according to UTF-8.
			OutputStreamWriter osw = new OutputStreamWriter(
					socket.getOutputStream(), encoder.getCharset());
			while ((line = br.readLine()) != null) {
				// make sure to add the FRAMING
				osw.write(line + "\n");
				osw.flush();
			}
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: java TcpClient host port");
			return;
		}
		run(args[0], Integer.parseInt(args[1]));
	}
}