package Tokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;




import java.util.HashMap;

import Encoder.SimpleEncoder;
import Stomp.*;

public class StompTokenizerImpl implements StompTokenizer {

	public final char delimiter;
	private BufferedReader in;
	private PrintWriter out;
	private boolean closed;

	public StompTokenizerImpl(BufferedReader in, char delimiter) {
		this.delimiter = delimiter;
		closed = false;
		this.in = in;
	}

	public void init(Socket clientSocket, SimpleEncoder simpleEncoder) throws IOException {
		out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8"), true);
		
	}

	public StompFrame getFrame(BufferedReader br) {
		StompFrame stompFrame;
		if (!isAlive())
			System.out.println("tokenizer is closed" + new IOException());
        String msg = null;
        try {
            // we are using a blocking stream, so we should always end up
            // with a message, or with an exception indicating an error in
            // the connection.
            int c;
            StringBuilder sb = new StringBuilder();
            // read char by char, until encountering the framing character, or
            // the connection is closed.
            while ((c = br.read()) != -1) {
                if (c == delimiter){
                    break;
                }
                else{
                    sb.append(String.valueOf((char) c));
                	
                }
            }
            msg = sb.toString();
        } catch (IOException e) {
            closed = true;
            System.out.println("Connection is dead");
        }
		return handleMessage(msg);
	}

	public StompFrame handleMessage(String msg){
		String message = msg;
		//checks until first space, then a lot of if's and creates the needed frame call the tokenizers "must be implemented method"
		int endLine = message.indexOf('\n');	// index of first ' ' in String msg.
		String userCommand = message.substring(0, endLine);
		Command command = new Command(userCommand);
		message = message.substring(endLine + 1, message.length());
		StompFrame newFrame;
		if (userCommand.equals("CONNECT"))
			newFrame = new ConnectFrame(command, new HashMap<String,String>(), null);
		else if (userCommand.equals("SUBSCRIBE"))
			newFrame = new SubscribeFrame(command, null, userCommand);
		else if (userCommand.equals("UNSUBSCRIBE"))
			newFrame = new UnsubscribeFrame(command, null, userCommand);
		/*else if (v.equals("BEGIN"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("COMMIT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("ABORT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		else if (userCommand.equals("SEND"))
			newFrame = new SendFrame(command,message);
		/*else if (v.equals("MESSAGE"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("RECEIPT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("CONNECTED"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		else if (userCommand.equals("DISCONNECT"))
			newFrame = new DisconnectedFrame(command, null, userCommand);
		else  {
			newFrame = new ErrorFrame(command, null, userCommand);
		}
		newFrame.handleFrame(message);
		return newFrame;
		
	}
	

	@Override
	public boolean isAlive() {
		return !closed;
	}

	public StompFrame createFrame(Command command) {
		StompFrame newFrame;
		String commandType = command.toString();
		if (commandType.equals("CONNECT"))
			newFrame = new ConnectFrame(command, null, commandType);
		else if (commandType.equals("SUBSCRIBE"))
			newFrame = new SubscribeFrame(command, null, commandType);
		else if (commandType.equals("UNSUBSCRIBE"))
			newFrame = new UnsubscribeFrame(command, null, commandType);
		/*else if (v.equals("BEGIN"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("COMMIT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("ABORT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		else if (commandType.equals("SEND"))
			newFrame = new SendFrame(command,commandType);
		/*else if (v.equals("MESSAGE"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("RECEIPT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("CONNECTED"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		else if (commandType.equals("DISCONNECT"))
			newFrame = new DisconnectedFrame(command, null, commandType);
		else  {
			newFrame = new ErrorFrame(command, null, commandType);
		}
		return newFrame;
	}

	@Override
	public String nextToken() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BufferedReader getBr() {
		return in;
	}

}
