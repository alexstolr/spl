package Tokenizer;

import java.io.BufferedReader;
import java.io.IOException;

import Stomp.StompFrame;

/**
 * This interface care of tokenizing an input stream into protocol specific
 * messages.
 * 
 */
public interface StompTokenizer {
	
	public StompFrame getFrame(BufferedReader br);
	
	/**
     * @return the next token, or null if no token is available. Pay attention
     *         that a null return value does not indicate the stream is closed,
     *         just that there is no message pending.
     * @throws IOException to indicate that the connection is closed.
     */
    String nextToken() throws IOException;
 
    /**
     * @return whether the input stream is still alive.
     */
    boolean isAlive();
    
    public BufferedReader getBr();
    
}
