package Tokenizer;

import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;

public class StompTokenizerFactory implements ServerTokenizerFactory {

	@Override
	public StompTokenizerImpl create(BufferedReader in) throws UnsupportedEncodingException {
		// BufferedReader br = new BufferedReader(new
		// InputStreamReader(System.in,"UTF-8"));
		// InputStreamReader isr = new
		// InputStreamReader(client.getInputStream(), encoder.getCharset());
		return new StompTokenizerImpl(in, "@".charAt(0));
	}

}
