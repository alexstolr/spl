package Tokenizer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public interface ServerTokenizerFactory {

	StompTokenizer create(BufferedReader in) throws UnsupportedEncodingException;
}
