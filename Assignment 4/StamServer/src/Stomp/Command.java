package Stomp;

import java.io.OutputStream;
import java.io.IOException;

/**

 */
public class Command {
	public final static String ENCODING = "UTF-8";// TODO should this be UTF-8??
	private String _command;

	public Command(String msg) {
		_command = msg;

	}

	/*public Command() {
	}*/

	//TODO
	/*public static Command SEND = new Command("SEND");
	public static Command SUBSCRIBE = new Command("SUBSCRIBE");
	public static Command UNSUBSCRIBE = new Command("UNSUBSCRIBE");
	public static Command BEGIN = new Command("BEGIN");
	public static Command COMMIT = new Command("COMMIT");
	public static Command ABORT = new Command("ABORT");
	public static Command DISCONNECT = new Command("DISCONNECT");
	public static Command CONNECT = new Command("CONNECT");
	public static Command MESSAGE = new Command("MESSAGE");
	public static Command RECEIPT = new Command("RECEIPT");
	public static Command CONNECTED = new Command("CONNECTED");
	public static Command ERROR = new Command("ERROR");

	public static Command valueOf(String v) {
		v = v.trim();
		if (v.equals("SEND"))
			return SEND;
		else if (v.equals("SUBSCRIBE"))
			return SUBSCRIBE;
		else if (v.equals("UNSUBSCRIBE"))
			return UNSUBSCRIBE;
		else if (v.equals("BEGIN"))
			return BEGIN;
		else if (v.equals("COMMIT"))
			return COMMIT;
		else if (v.equals("ABORT"))
			return ABORT;
		else if (v.equals("CONNECT"))
			return CONNECT;
		else if (v.equals("MESSAGE"))
			return MESSAGE;
		else if (v.equals("RECEIPT"))
			return RECEIPT;
		else if (v.equals("CONNECTED"))
			return CONNECTED;
		else if (v.equals("DISCONNECT"))
			return DISCONNECT;
		else if (v.equals("ERROR"))
			return ERROR;
		throw new Error("Unrecognised command " + v);
	}*/
	
	@Override
	public String toString() {
		return _command;
	}
	
	
}
