package Stomp;

import java.util.Map;

public class DisconnectedFrame extends StompFrame {

	private Command _command;
	private Map _headers;
	private String _body;
	private String _receipt;

	public DisconnectedFrame(Command c, Map h, String b) {
		_command = c;
		_headers = h;
		_body = b;
		_receipt = "";
	}

	public Map headers() {
		return _headers;
	}

	public String body() {
		return _body;
	}

	public Command command() {
		return _command;
	}

	@Override
	public void handleFrame(String msg) {
		String message = msg;
		_receipt = message.substring(message.indexOf(":") + 1, message.indexOf('\n'));
	}
}
