package Stomp;

import java.util.Map;

public abstract class StompFrame {


	public abstract Map headers();

	public abstract String body();

	public abstract Command command();
	
	public abstract void handleFrame(String msg);
}
