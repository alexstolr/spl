package Stomp;

import java.util.Map;

public class SendFrame extends StompFrame {
	private Command _command;
	private Map _headers;
	private String _body;

	public SendFrame(Command c, String b) {
		_command = c;
		_headers = null;
		_body = b;
	}

	public Map headers() {
		return _headers;
	}

	public String body() {
		return _body;
	}

	public Command command() {
		return _command;
	}

	@Override
	public void handleFrame(String msg) {
		String _destination;
		String message = msg;
		
		_destination = message.substring(message.indexOf(":") + 1, message.indexOf('\n'));
		message= message.substring(message.indexOf('\n') + 2);
		message = message.substring(0,message.indexOf('\n'));
	}
}
