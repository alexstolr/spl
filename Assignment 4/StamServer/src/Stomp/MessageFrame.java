package Stomp;

import java.util.Map;

public class MessageFrame extends StompFrame {
	private Command _command;
	private Map _headers;
	private String _body;

	public MessageFrame(Command c, Map h, String b) {
		_command = c;
		_headers = h;
		_body = b;
	}

	public Map headers() {
		return _headers;
	}

	public String body() {
		return _body;
	}

	public Command command() {
		return _command;
	}

	@Override
	public void handleFrame(String msg) {
		// TODO Auto-generated method stub
		
	}
}
