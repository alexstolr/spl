package Stomp;

import java.util.Map;

public class SubscribeFrame extends StompFrame {
	private Command _command;
	private Map _headers;
	private String _body;
	private String _id;
	private String _destination;

	public SubscribeFrame(Command c, Map h, String b) {
		_command = c;
		_headers = h;
		_body = b;
		_id = "";
		_destination = "";
	}

	public Map headers() {
		return _headers;
	}

	public String body() {
		return _body;
	}

	public Command command() {
		return _command;
	}

	@Override
	public void handleFrame(String msg) {
		String message = msg;
		_destination = message.substring(message.indexOf(":") + 1, message.indexOf('\n'));
		message= message.substring(message.indexOf('\n') + 1);
		_id = message.substring(message.indexOf(":") + 1, message.indexOf('\n'));
	}
}
