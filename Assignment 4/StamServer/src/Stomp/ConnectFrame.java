package Stomp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConnectFrame extends StompFrame {

	private Command _command;
	private Map<String, String> _headers;
	private String _body;

	public ConnectFrame(Command c, HashMap h, String body) {
		_command = c;
		_headers = new HashMap<String, String>();
		_body = body;
	}

	public String getName(String name) {
		return _headers.get(name);
	}

	public String body() {
		return _body;
	}

	public Command command() {
		return _command;
	}

	@Override
	public void handleFrame(String msg) {
		String message = msg;
		String userName, passCode;
		for (int i = 0; i < 2; i++) {
			message = message.substring(message.indexOf('\n') + 1,message.length());
		}
		userName = message.substring(message.indexOf(":") + 1,message.indexOf('\n'));
		message = message.substring(message.indexOf('\n') + 1, message.length());
		passCode = message.substring(message.indexOf(':') + 1,message.indexOf('\n'));
		_headers.put(userName, passCode);
		test();
	}

	@Override
	public Map headers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		test();
		return "ConnectFrame [_command=" + _command + ", _headers="
				+ ", _body=" + _body + "]";
	}

	public void test() {
		List<String> keys = new ArrayList<String>(_headers.keySet());
		for (String key : keys) {
			System.out.println("username ---> " + key + '\n' + "passcode ---> " + _headers.get(key));
		}
	}

}
