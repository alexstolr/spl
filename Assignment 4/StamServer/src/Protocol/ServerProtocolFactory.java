package Protocol;

public interface ServerProtocolFactory {
	ServerProtocol create();
}
