package Protocol;

public class StompProtocolFactory implements ServerProtocolFactory {

	@Override
	public StompProtocol create() {
		return new StompProtocol();
	}

}
