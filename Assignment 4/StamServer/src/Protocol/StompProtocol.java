package Protocol;

import Stomp.Command;
import Stomp.ConnectFrame;
import Stomp.DisconnectedFrame;
import Stomp.ErrorFrame;
import Stomp.SendFrame;
import Stomp.StompFrame;
import Stomp.SubscribeFrame;
import Stomp.UnsubscribeFrame;
import Tokenizer.StompTokenizerImpl;

public class StompProtocol implements ServerProtocol {
	//TODO this is like reciever in gozzira
	private boolean shouldClose;
	private int lineNumber;

	public StompProtocol() {
		shouldClose = false;
		lineNumber = 0;
	}

	@Override
	public String processMessage(StompTokenizerImpl tokenizer, String msg) {
		String message = msg;
		//checks until first space, then a lot of if's and creates the needed frame call the tokenizers "must be implemented method"
		System.out.println(message);//TODO
		int space = message.indexOf('\n');	// index of first ' ' in String msg.
		String userCommand = message.substring(0, space);
		message = message.substring(space + 1, message.length());
		System.out.println(message);
		Command command = new Command(userCommand);
		StompFrame newFrame;
		if (userCommand.equals("CONNECT"))
			newFrame = new ConnectFrame(command, null, userCommand);
		else if (userCommand.equals("SUBSCRIBE"))
			newFrame = new SubscribeFrame(command, null, userCommand);
		else if (userCommand.equals("UNSUBSCRIBE"))
			newFrame = new UnsubscribeFrame(command, null, userCommand);
		/*else if (v.equals("BEGIN"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("COMMIT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("ABORT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		else if (userCommand.equals("SEND"))
			newFrame = new SendFrame(command,userCommand);
		/*else if (v.equals("MESSAGE"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("RECEIPT"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		/*else if (v.equals("CONNECTED"))
			newFrame = new ConnectedFrame(command, null, commandType);*/
		else if (userCommand.equals("DISCONNECT"))
			newFrame = new DisconnectedFrame(command, null, userCommand);
		else  {
			newFrame = new ErrorFrame(command, null, userCommand);
		}
		
		return null;
	}

	@Override
	public boolean isEnd(String msg) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean shouldClose() {
		return shouldClose;
	}

	@Override
	public void connectionTerminated() {
		shouldClose = true;
	}

}
