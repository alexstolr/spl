/*
 * Client.cpp
 *
 *  Created on: Jan 5, 2014
 *      Author: alexstol
 */

#include "../include/Client.h"
#include "../include/Receiver.h"


Client::Client(string host, short port,int _Receipt_id):Receipt_id(_Receipt_id),host_(host), port_(port), io_service_(), socket_(io_service_),_connected(false){}
void Client::initUserNameOfTheClient(string username){_username=username;}
string Client::getUsername(){return _username;}
Client::~Client() {
    close();
}
bool Client::isConnected(){return _connected;}
void Client::disconnect(){_connected=false;}

bool Client::connect() {
    cout << "Starting connect to "
        << host_ << ":" << port_ << endl;
    try {
		tcp::endpoint endpoint(boost::asio::ip::address::from_string(host_), port_); // the server endpoint
		boost::system::error_code error;
		socket_.connect(endpoint, error);
		if (error)
			throw boost::system::system_error(error);

    }
    catch (exception& e) {
        cerr << "Connection failed (Error: " << e.what() << ')' << endl;
        return false;
    }
    return true;
}

string Client::getReceiptId(){
	stringstream stringStream;
	stringStream << Receipt_id;
	string str = stringStream.str();
	return str;
}

bool Client::getBytes(char bytes[], unsigned int bytesToRead) {
    size_t tmp = 0;
	boost::system::error_code error;
    try {
        while (!error && bytesToRead > tmp ) {
			tmp += socket_.read_some(boost::asio::buffer(bytes+tmp, bytesToRead-tmp), error);
        }
		if(error)
            throw boost::system::system_error(error);
    } catch (exception& e) {
        cerr << "receive failed (Error: " << e.what() << ')' << std::endl;
        return false;
    }
    return true;
}

bool Client::sendBytes(const char bytes[], int bytesToWrite) {
    int tmp = 0;
    boost::system::error_code error;
        try {
            while (!error && bytesToWrite > tmp ) {
    			tmp += socket_.write_some(boost::asio::buffer(bytes + tmp, bytesToWrite - tmp), error);
            }
    		if(error)
    			throw boost::system::system_error(error);
    } catch (exception& e) {
        cerr << "recv failed (Error: " << e.what() << ')' << endl;
        return false;
    }
    return true;
}

bool Client::getLine(string& line) {
    return getFrameAscii(line, '\n');
}

bool Client::sendLine(string& line) {
    return sendFrameAscii(line, '\n');
}

bool Client::getFrameAscii(string& frame, char delimiter) {
    char ch;
    // Stop when we encounter the delimiter character.
    // Notice that the delimiter is appended to the frame string.
    try {
        do{
            getBytes(&ch, 1);
            frame.append(1, ch);
        }while (delimiter != ch);
    } catch (exception& e) {
        cerr << "recv failed (Error: " << e.what() << ')' << endl;
        return false;
    }
    return true;
}

bool Client::sendFrameAscii(const string& frame, char delimiter) {
    bool result=sendBytes(frame.c_str(),frame.length());
    if(!result) return false;
    return sendBytes(&delimiter,1);
}

// Close down the connection properly.
void Client::close() {
    try{
        socket_.close();
    } catch (...) {
        std::cout << "closing failed: connection already closed" << std::endl;
    }
}



