/*
 * EchoClient.cpp
 *
 *  Created on: Jan 15, 2014
 *      Author: talmonda
 */
#include <stdlib.h>
#include <boost/locale.hpp>
#include <iostream>
#include <boost/thread.hpp>
#include "../include/Client.h"
#include "../include/Receiver.h"
#include "../include/NetListener.h"
#include "../include/ConnectFrame.h"


int main(int argc, char *argv[]){
	//
	if (argc < 4) {
		std::cerr << "Usage: " << argv[0] << " host port" << std::endl << std::endl;
		return -1;
	}
	std::string host = argv[1];
	unsigned short port = atoi(argv[2]);
	unsigned short receipt_id = atoi(argv[3]);

	Client* connectionHandler=new Client(host, port,receipt_id);
	if (!connectionHandler->connect()) {
		std::cerr << "Cannot connect to " << host << ":" << port << std::endl;
		return 1;
	}

	std::cout<<"welcome,input your user details:"<<std::endl;
	 while (1) {
	        const short bufsize = 1024;
	        char buf[bufsize];
	        std::cin.getline(buf, bufsize);
	        std::string login_details(buf);
	        if (!connectionHandler->sendFrameAscii(login_details,'\0')) {
	            std::cout << "Disconnected. Exiting...\n" << std::endl;
	            break;
	        }
	        NetListener task1(connectionHandler);
	        boost::thread th1(&NetListener::run,&task1);
	        Receiver reciever(connectionHandler);
	        reciever.receive();
}

}


