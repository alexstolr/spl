/*
 * Receiver.cpp
 *
 *  Created on: Jan 7, 2014
 *      Author: alexstol
 */
#include"../include/Receiver.h"
#include"../include/SendFrame.h"
#include"../include/SubscribeFrame.h"
#include"../include/DisconnectFrame.h"
#include <iostream>


Receiver::Receiver( Client* m) {
	_receiver = m;
}
void Receiver::receive() {
	while(_receiver->isConnected()){
		const short bufsize = 1024;
		char buf[bufsize];
		 string message;
		std::cin.getline(buf, bufsize);
		std::string line(buf);
		int len=line.length();
		if(line.substr(0,line.find(" ")).compare("twit")){
			StompFrame* frame=new SendFrame(line.substr(line.find(" ")));
			frame->prapreTheFrame(message);
			delete frame;
		}
		if(line.substr(0,line.find(" ")).compare("clients")){
			StompFrame* frame=new SendFrame(line);
			frame->prapreTheFrame(message);
			delete frame;
		}
		if(line.substr(0,line.find(" ")).compare("follow")){
			StompFrame* frame=new SubscribeFrame(line);
			frame->prapreTheFrame(message);
			delete frame;
		}
		if(line.compare("client_exit")){
				std::cout << "exit gracefully" << std::endl;
				_receiver->disconnect();
				_receiver->close();
			}
		if(line.substr(0,line.find(" ")).compare("unfollow")){
			StompFrame* frame=new SubscribeFrame(line);
			frame->prapreTheFrame(message);
			delete frame;
		}
		if(line.substr(0,line.find(" ")).compare("logout")){
			StompFrame* frame=new DisconnectFrame(line);
			message=message+message+" "+_receiver->getReceiptId();
			frame->prapreTheFrame(message);
			delete frame;
		}
		if(!_receiver->sendFrameAscii(message,'\0')){
			 break;
		}
	}

}
Receiver::~Receiver(){
	delete _receiver;
}





