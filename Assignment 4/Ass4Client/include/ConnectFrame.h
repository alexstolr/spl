/*
 * ConnectFrame.h
 *
 *  Created on: Jan 12, 2014
 *      Author: talmonda
 */

#ifndef CONNECTFRAME_H_
#define CONNECTFRAME_H_
#include "../include/Client.h"
#include "../include/StompFrame.h"
#include<string>
class ConnectFrame:public StompFrame{
private:
	string  _body;
public:
	ConnectFrame(string  body);
	void prapreTheFrame(string& frame);
	~ConnectFrame();
};



#endif /* CONNECTFRAME_H_ */
