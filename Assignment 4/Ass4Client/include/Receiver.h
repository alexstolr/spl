/*
 * Receiver.h
 *
 *  Created on: Jan 6, 2014
 *      Author: alexstol
 */


#ifndef RECEIVER_H_
#define RECEIVER_H_

#include "..//include//Client.h"

using namespace std;

class Receiver{
private:
Client* _receiver;
public:
Receiver(Client* m);
void receive();
~Receiver();
};


#endif /* RECEIVER_H_ */
