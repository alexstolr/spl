/*
 * SendFrame.h
 *
 *  Created on: Jan 9, 2014
 *      Author: alexstol
 */
#ifndef SENDFRAME_H_
#define SENDFRAME_H_

#include "../include/Client.h"
#include "../include/StompFrame.h"

class SendFrame:public StompFrame{
private:
	string  _body;
public:
	SendFrame(string body);
	void virtual prapreTheFrame(string& frame);
	~SendFrame();
};


#endif /* SENDFRAME_H_ */
