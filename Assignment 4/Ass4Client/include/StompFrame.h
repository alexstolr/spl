/*
 * Stomp.h
 *
 *  Created on: Jan 6, 2014
 *      Author: alexstol
 */


#ifndef STOMP_FRAME_H_
#define STOMP_FRAME_H_

#include <map>
#include <hash_map>
#include <string>
#include <vector>
#include <list>
#include <stack>

using namespace std;

class StompFrame{
public:
virtual void prapreTheFrame(std::string& frame)=0;
virtual ~StompFrame();

};



#endif /* STOMP_H_ */
