/*
 * Client.h
 *
 *  Created on: Jan 5, 2014
 *      Author: alexstol
 */
#ifndef CLIENT_H_
#define CLIENT_H_

#include "../include/StompFrame.h"
#include <string>
#include <iostream>
#include<hash_map>
#include <boost/asio.hpp>
#include <boost/thread.hpp>



using boost::asio::ip::tcp;
using namespace std;



class Client{
private:
	int Receipt_id;
     string _username;
     string host_;
     short port_;
	boost::asio::io_service io_service_;   // Provides core I/O functionality
	tcp::socket socket_;
    bool _connected;

public:
    Client(string host, short port,int _Receipt_id);
    void initUserNameOfTheClient(string username);
    string getUsername();
    virtual ~Client();
    bool isConnected();
    // Connect to the remote machine
    bool connect();

    // Read a fixed number of bytes from the server - blocking.
    // Returns false in case the connection is closed before bytesToRead bytes can be read.
    bool getBytes(char bytes[], unsigned int bytesToRead);

    // Send a fixed number of bytes from the client - blocking.
    // Returns false in case the connection is closed before all the data is sent.
    bool sendBytes(const char bytes[], int bytesToWrite);

    // Read an ascii line from the server
    // Returns false in case connection closed before a newline can be read.
    bool getLine(std::string& line);

    // Send an ascii line from the server
    // Returns false in case connection closed before all the data is sent.
    bool sendLine(std::string& line);

    // Get Ascii data from the server until the delimiter character
    // Returns false in case connection closed before null can be read.
    bool getFrameAscii(std::string& frame, char delimiter);

    // Send a message to the remote host.
    // Returns false in case connection is closed before all the data is sent.
    bool sendFrameAscii(const std::string& frame, char delimiter);

    // Close down the connection properly.
    void close();
	bool isClosed();
	void disconnect();
	string getReceiptId();
};


#endif /* CLIENT_H_ */
