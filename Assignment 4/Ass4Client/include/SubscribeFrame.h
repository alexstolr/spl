/*
 * SubscribeFrame.h
 *
 *  Created on: Jan 9, 2014
 *      Author: talmonda
 */

#ifndef SUBSCRIBEFRAME_H_
#define SUBSCRIBEFRAME_H_
#include"../include/StompFrame.h"

class SubscribeFrame:public StompFrame{
private:

	string  _body;

public:

	SubscribeFrame(string  body);
	void prapreTheFrame(string& frame);
	~SubscribeFrame();

};

#endif /* SUBSCRIBEFRAME_H_ */
