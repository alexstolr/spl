/*
 * DisconnctFrame.h
 *
 *  Created on: Jan 12, 2014
 *      Author: talmonda
 */

#ifndef DISCONNCTFRAME_H_
#define DISCONNCTFRAME_H_
#include "../include/Client.h"
#include "../include/StompFrame.h"
class DisconnectFrame:public StompFrame{
private:
	string  _body;
public:
	DisconnectFrame(string  body);
	void prapreTheFrame(string& frame);
	~DisconnectFrame();
};


#endif /* DISCONNCTFRAME_H_ */
