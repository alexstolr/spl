/*
 * UnsubscribeFrame.h
 *
 *  Created on: Jan 12, 2014
 *      Author: talmonda
 */

#ifndef UNSUBSCRIBEFRAME_H_
#define UNSUBSCRIBEFRAME_H_
#include "../include/Client.h"
#include "../include/StompFrame.h"

class UnsubscribeFrame:public StompFrame{
private:
	string  _body;
public:
	UnsubscribeFrame(string  body);
	void prapreTheFrame(string& frame);
	~UnsubscribeFrame();
};
#endif /* UNSUBSCRIBEFRAME_H_ */
