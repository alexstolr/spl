/*
 * NetListener.h
 *
 *  Created on: Jan 9, 2014
 *      Author: alexstol
 */
#ifndef NETLISTENER_H_
#define NETLISTENER_H_
#include"../include/Client.h"
using namespace std;

 class NetListener{
private:

Client* _client;

public:
NetListener(Client* c);
void run();
~NetListener();
 };

#endif /* NETLISTENER_H_ */
