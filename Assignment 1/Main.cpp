//add comments
#include "..//include//Assignment1.h"
using namespace std;

int main(int argc, char **argv) {
	string prodLine;
	string suppLine;
	vector <vector<string> > prodVec;
	vector<vector<string> > suppVec;

	fstream products;
	products.open("Products.conf");
	fstream suppliers;
	suppliers.open("Suppliers.conf");
	if (products.is_open()){

		while (!products.eof()){  // while not finished going threw the whole file.
			getline(products, prodLine);  // puts first line from Products.conf to prodLine.
			vector<string> newVec;
			prodLine = prodLine + ',';
			while (prodLine.length()-1 != prodLine.find(',') ){
				newVec.push_back(prodLine.substr(0,prodLine.find(',')));
				prodLine = prodLine.substr(prodLine.find(',')+1);
			}
			newVec.push_back(prodLine.substr(0,prodLine.find(',')));
			prodLine = prodLine.substr(prodLine.find(',')+1);
			prodVec.push_back(newVec);
			prodLine = "";
		}

		products.close(); // closes for reading of products
	}
	int row=1;
	vector<string> row1;
	suppVec.push_back(row1);
	if (suppliers.is_open()){
		while(!suppliers.eof()){
			getline(suppliers, suppLine);
			vector<string> rowi;
			suppVec.push_back(rowi);
			suppVec.at(row).push_back(suppLine.substr(0,suppLine.find(',')));
			suppLine=suppLine.substr(suppLine.find(',')+1);  //prodLine doesnt include the product.
			suppVec.at(0).push_back(suppLine.substr(0,suppLine.find(',')));
			suppLine=suppLine.substr(suppLine.find(',')+1);
			suppVec.at(row).push_back(suppLine.substr(0,suppLine.find(',')));
			row++;
		}
		suppliers.close();
	}

	//printProdVector(prodVec);

	//printSuppVector(suppVec);


	okProducts(prodVec,suppVec);
}
