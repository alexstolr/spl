#ifndef Assignment1_H_
#define Assignment1_H_
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <istream>
using namespace std;

//vector <vector<string> > suppVec;

void okProducts(vector <vector<string> > prodVec, vector<vector<string> > suppVec);
double findIngredientPrice(string ingredient, vector<vector<string> > suppVec);  //finds ingredients cheapest price.
string findIngredientSupplier(string ingredient, vector<vector<string> > suppVec, double price);   // returns suppliers of cheapest ingredient.
void printProdVector(vector <vector<string> > prodVec);
void printSuppVector(vector <vector<string> > suppVec);

//double find_Min_Product(vector <string> supp, int SizeP, int SizeS, string Ingredient, fstream ShoppingList);
//void the_Product_Price(vector <string> Product, vector <string> supp, int SizeS, fstream ShoppingList, fstream Menu);

#endif
