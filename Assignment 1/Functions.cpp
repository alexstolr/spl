#include <vector>
#include "../include/Assignment1.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits>
using namespace std;

void okProducts(vector <vector<string> > prodVec, vector<vector<string> > suppVec){
	double price;
	bool flag = true; //bool for the first time that we insert supplier to the ShoppingList
	string product;
	vector <vector<string> > outputSuppliers;
	vector<string> v1;
	outputSuppliers.push_back(v1);
	ofstream menuOutputFile;
	ofstream shoppingListOutput;
	int row=0;
	menuOutputFile.open("Menu.out");
	for (size_t col = 0; col < prodVec.size(); col++){ // runs on each row in prodVec.
		price = 0;
		product = prodVec.at(col).at(0); // puts the product of the current column in product.
		for (size_t rowi = 1; rowi < prodVec.at(col).size(); rowi++){ //runs on each row inside the current column, starting from row 1 to the end.
			price = price + findIngredientPrice((prodVec.at(col)).at(rowi), suppVec); // sets price to be = current price + price of added ingredient.
		}
		if (((price + 1)*1.5) <= 5 && strcmp(product.c_str(),(""))){
			menuOutputFile << product << ',' << ((price + 1)*1.5) << '\n';  //write to file Menu.out and do \n .
			for (size_t countOfIngreds = 1;countOfIngreds < prodVec.at(row).size(); countOfIngreds++){
				double currentIngredientPrice = findIngredientPrice(prodVec.at(row).at(countOfIngreds), suppVec); // finds lowest price for current ingredient.
				string currentSupplier = findIngredientSupplier(prodVec.at(col).at(countOfIngreds), suppVec, currentIngredientPrice); //finds the cheapest supplier of the current ingridient
				//now need to search for supplier in the vector, if not found create new.
				bool found = false;
				bool found1 = false;
				if(flag==true){// because the first  vector does not exist so (1) is the error
					vector<string> v;
					outputSuppliers.push_back(v);
					outputSuppliers.at(0).push_back(currentSupplier);
					outputSuppliers.at(1).push_back(prodVec.at(row).at(countOfIngreds));
					flag=false;
				}
				else// not the first supplier's Ingredient that can be insert to outputSuppliers vector
				{
					for (size_t n = 0; n < outputSuppliers.at(0).size() && !found; n++){
						if (strcmp(outputSuppliers.at(0).at(n).c_str(), currentSupplier.c_str())==0){//(1)
							found=true;
							for(size_t x = 0;x < outputSuppliers.at(n+1).size();x++)
							{
								if(strcmp(outputSuppliers.at(n+1).at(x).c_str(),prodVec.at(row).at(countOfIngreds).c_str())==0)
								{
									found1=true;
								}
							}
							if(!found1)outputSuppliers.at(n+1).push_back(prodVec.at(row).at(countOfIngreds));
						}

					}
					if(!found){
						vector<string> v;
						outputSuppliers.push_back(v);
						outputSuppliers.at(0).push_back(currentSupplier);
						outputSuppliers.at(outputSuppliers.size()-1).push_back(prodVec.at(row).at(countOfIngreds));
					}
					found = false;
				}
			}
		}
		row++;
	}

	shoppingListOutput.open("ShoppingList.out");
	for (size_t i = 0; i < outputSuppliers.at(0).size(); ++i){   shoppingListOutput << outputSuppliers.at(0).at(i);
	for (size_t j = 0; j < outputSuppliers.at(i+1).size(); ++j){
		shoppingListOutput << ',' << outputSuppliers.at(i+1).at(j) ;
	}
	shoppingListOutput << '\n';
	}
	menuOutputFile.close();
}

double findIngredientPrice(string ingredient, vector<vector<string> > suppVec){   //finds ingredients cheapest price.
	double price = 10;
	//
	for (size_t i = 0; i < suppVec.at(0).size(); i++ ){ // runs on all the ingredient
		if (atof((suppVec.at(i+1).at(1)).c_str()) < price && strcmp(ingredient.c_str(),suppVec.at(0).at(i).c_str())==0)
			price = atof((suppVec.at(i+1).at(1)).c_str());
	}
	return price;
}

string findIngredientSupplier(string ingredient, vector<vector<string> > suppVec, double price){    //finds supplier of the ingredient's cheapest price.
	string supplier;
	bool found = false;
	for (size_t i = 0; i < suppVec.at(0).size()&&!found; i++){
		if (strcmp(ingredient.c_str(), suppVec.at(0).at(i).c_str())==0){
			if (price == atof((suppVec.at(i+1).at(1)).c_str())){
				supplier = suppVec.at(i+1).at(0);
				found = true;
			}
		}
	}
	return supplier;
}

void printProdVector(vector <vector<string> > prodVec){
	cout << "will print out : product,ing1,ing2,...,ingn,  for each product "<< endl<<endl;
	for(size_t col = 0; col < prodVec.size(); col++) {               //test print out vector
		cout << prodVec.at(col).at(0)<< ":";
		for(size_t j = 1; j < prodVec.at(col).size(); j++){
			cout << (prodVec.at(col)).at(j) << ",";;
		}
		cout << endl;
	}
}

void printSuppVector(vector <vector<string> > suppVec){
	cout << "will print out : product1,product2,......,productn "<< endl;
	cout << "supplier of product1, price of product1 given by this supplier"<<endl;
	cout << "supplier of product2, price of product2 given by this supplier"<<endl;
	cout << "   .           .                  .                           "<<endl;
	cout << "   .           .                  .                           "<<endl;
	cout << "   .           .                  .                           "<<endl;
	cout << "supplier of productn, price of productn given by this supplier"<<endl << endl;
	for(size_t i = 0 ; i < suppVec.size() ; i++){
			for(size_t j = 0 ; j < suppVec.at(i).size() ; j++)
				cout << suppVec.at(i).at(j) << "," ;
			cout << endl;
		}
}

